import 'dart:developer';

import 'package:frontend_ui/views/AccountScreen/account.dart';
import 'package:frontend_ui/views/CartScreen/cart.dart';
import 'package:frontend_ui/views/HomeScreen/home.dart';
import 'package:frontend_ui/views/LoginScreen/login_screen.dart';
import 'package:frontend_ui/views/MainComponents/bottombar.dart';
import 'package:frontend_ui/views/SignUpScreen/signuo_screen.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/addReview.dart';
import 'package:frontend_ui/views/SingleProductScreen/single_product_screen.dart';
import 'package:frontend_ui/views/addAddress/addAddress.dart';
import 'package:frontend_ui/views/allProducts/all_Products.dart';
import 'package:frontend_ui/views/checkoutScreen/checkoutPayment.dart';
import 'package:frontend_ui/views/checkoutScreen/checkoutReview.dart';
import 'package:frontend_ui/views/checkoutScreen/checkoutShipping.dart';
import 'package:frontend_ui/views/confirmOrder/confirmOrder.dart';
import 'package:frontend_ui/views/productsByCategory/products_by_category.dart';
import 'package:frontend_ui/views/savedAddress/savedAddress.dart';

getRoutes() {
  return {
    //   '/': (ctx) => LandingPage(),
    LoginScreen.route: (ctx) => LoginScreen(),
    SignUpScreen.route: (ctx) => SignUpScreen(),
    AccountScreen.route: (ctx) => AccountScreen(),
    AllProducts.route: (ctx) => AllProducts(),
    SingleProductScreen.route: (ctx) => SingleProductScreen(),
    ProductsByCategory.route: (ctx) => ProductsByCategory(),
    CartScreen.route: (ctx) => CartScreen(),
    HomeScreen.route: (ctx) => HomeScreen(),
    CheckoutPayment.route: (ctx) => CheckoutPayment(),
    CheckoutShipping.route: (ctx) => CheckoutShipping(),
    CheckoutReview.route: (ctx) => CheckoutReview(),
    ConfirmedOrder.route: (ctx) => ConfirmedOrder(),
    BottomNavBar.route: (ctx) => BottomNavBar(),
    AddAddress.route: (ctx) => AddAddress(),
    SavedAddress.route: (ctx) => SavedAddress(),
    AddReview.route: (ctx) => AddReview(),
  };
}
