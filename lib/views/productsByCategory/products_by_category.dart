
import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/categoryController.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/Widgets/product_block.dart';
import 'package:frontend_ui/models/routesArgs.dart';
import 'package:frontend_ui/views/MainComponents/drawer_global.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/SingleProductScreen/single_product_screen.dart';
import 'package:provider/provider.dart';

class ProductsByCategory extends StatefulWidget {
  const ProductsByCategory({Key? key}) : super(key: key);
  static String route = "ProductsByCategory";

  @override
  _ProductsByCategoryState createState() => _ProductsByCategoryState();
}

class _ProductsByCategoryState extends State<ProductsByCategory> {
  var args;
  @override
  void initState() {
    super.initState();
    final productsItem =
        Provider.of<ProductByCategoriesController>(context, listen: false);
    final categoryChildren =
        Provider.of<CategoryController>(context, listen: false);
    Future.delayed(Duration.zero, () {
      setState(() {
        final args = ModalRoute.of(context)!.settings.arguments
            as ProductsByCategoryArgs;
        // print(args.id.toString());
        categoryChildren.fetchCateChildren(args.id.toString());
        productsItem.findAllProductOfId(categoryChildren.categoryByParentID);
  
      });
    });
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final categoryChildren =
        Provider.of<CategoryController>(context, listen: false);
    final productsItem = Provider.of<ProductByCategoriesController>(context);
    final args =
        ModalRoute.of(context)!.settings.arguments as ProductsByCategoryArgs;
    final productName = args.name;
    // print(productCateId);
    // productsItem.find(context, productCateId.toString());

    // print(productsByCategoryList);
    return Scaffold(
      appBar: appBarBack(context, productName.toString(), () {
        Navigator.pop(context);
      }),
      drawer: AppDrawer(),
      body: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 190 / 250,
        children: List.generate(
          productsItem.proByIds.length,
          (index) => InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                SingleProductScreen.route,
                arguments: productsItem.proByIds[index].id,
              );
            },
            child: ProductBlock(
              id: productsItem.proByIds[index].id,
              name: productsItem.proByIds[index].name,
              description: productsItem.proByIds[index].description,
              price: productsItem.proByIds[index].price,
              quantity: productsItem.proByIds[index].quantity,
              productImages: productsItem.proByIds[index].productImages,
            ),
          ),
        ),
      ),
    );
  }
}
