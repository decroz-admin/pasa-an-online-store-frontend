import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/services/apiServices.dart';
import 'package:frontend_ui/views/AccountScreen/account.dart';
import 'package:frontend_ui/views/LoginScreen/login_screen.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:provider/provider.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);
  static String route = "Signup";

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String email = "";
  String lname = "";
  String contact = "";
  String fname = "";
  String password = "";
  String repassword = "";
  @override
  void initState() {
    final checker = Provider.of<VerificationChecker>(context, listen: false);
    checker.isLoggedIn();
    if (checker.isloggedIn != false) {
      Navigator.pushNamed(context, AccountScreen.route);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _signupFormKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: appBarBack(context, "SignUp", () {
        Navigator.pop(context);
      }),
      body: SingleChildScrollView(
        child: Form(
          key: _signupFormKey,
          child: Column(
            children: [
              Card(
                elevation: 5,
                margin: EdgeInsets.fromLTRB(20, 15, 20, 0),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Column(
                    children: [
                      Image.asset(
                        "assets/a.png",
                        width: double.infinity,
                        height: 100,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              textFieldLabel("First Name"),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 135,
                                child: textFieldForForm(
                                    "Hari", TextInputType.name, Icons.person,
                                    (value) {
                                  fname = value;
                                }),
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              textFieldLabel("Last Name"),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 135,
                                child: textFieldForForm(
                                    "Nepal", TextInputType.name, Icons.person,
                                    (value) {
                                  lname = value;
                                }),
                              )
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldLabel("Email"),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldForForm(
                          "example@example.com",
                          TextInputType.emailAddress,
                          Icons.email_outlined, (value) {
                        email = value;
                      }),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldLabel("Password"),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldForForm(
                          "Text you don't want to forget",
                          TextInputType.visiblePassword,
                          Icons.password_outlined, (value) {
                        password = value;
                      }),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldLabel("Re-enter password"),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldForForm(
                          "Text you don't want to forget",
                          TextInputType.visiblePassword,
                          Icons.password_outlined, (value) {
                        repassword = value;
                      }),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldLabel("Phone"),
                      SizedBox(
                        height: 10,
                      ),
                      textFieldForForm("9860463077", TextInputType.number,
                          Icons.password_outlined, (value) {
                        contact = value;
                      }),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: TextButton(
                  onPressed: () async {
                    if (password == repassword) {
                      if (_signupFormKey.currentState!.validate()) {
                        String result = await userSignup(
                            fname, lname, email, password, contact);
                        if (result == "SignUp Successful") {
                          // naviagate
                          Navigator.pushNamed(context, LoginScreen.route);
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("Signup Successful")),
                          );
                        } else {
                          //show error
                          // ScaffoldMessenger.of(context).showSnackBar(
                          //   SnackBar(content: Text(result.toString())),
                          // );
                        }
                      }
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("Password doesn't match")),
                      );
                    }
                  },
                  style: TextButton.styleFrom(
                    minimumSize: Size(double.infinity, 15),
                    primary: Colors.black,
                    backgroundColor: primaryColor,
                    elevation: 5,
                  ),
                  child: Text(
                    "Sign up",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 18),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
