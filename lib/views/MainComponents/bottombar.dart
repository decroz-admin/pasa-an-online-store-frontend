import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/navController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/AccountScreen/account.dart';
import 'package:frontend_ui/views/CartScreen/cart.dart';
import 'package:frontend_ui/views/HomeScreen/home.dart';
import 'package:frontend_ui/views/OrderScreen/order.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:provider/provider.dart';

class BottomNavBar extends StatelessWidget {
  BottomNavBar({Key? key}) : super(key: key);

  static String route = "mainScreen";
//   @override
//   _BottomNavBarState createState() => _BottomNavBarState();
// }

// class _BottomNavBarState extends State<BottomNavBar> {
  // var _pages;
  // int _selectedPageIndex = 0;

  //    @override
  // void initState() {
  // var _pages = [
  //     HomeScreen(),
  //     CartScreen(),
  //     OrderScreen(),
  //     AccountScreen(),
  //   ];
  //   super.initState();
  // }

  // void _selectPage(int index) {
  //   setState(() {
  //       _selectedPageIndex = index;
  //   });
  // }

  // @override
  Widget build(BuildContext context) {
    // int args = ModalRoute.of(context)!.settings.arguments as int;
    // passedvalue = args;

    final navController = Provider.of<NavController>(context);

    var _pages = [
      HomeScreen(),
      CartScreen(),
      OrderScreen(),  
      AccountScreen(),
    ];
    return Scaffold(
      body: _pages[navController.selectedPagesIndex],
      // body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: GNav(
        selectedIndex: navController.selectedPagesIndex,
        onTabChange: (index) {
          navController.currentIndex(index);
        },
        gap: 8,
        color: Colors.grey[800],
        activeColor: primaryColor,
        iconSize: 24,
        tabBackgroundColor: Colors.purple.withOpacity(0.2),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        duration: Duration(milliseconds: 900),
        tabs: [
          GButton(
            icon: Icons.home,
            text: 'Home',
          ),
          GButton(
            icon: Icons.shopping_cart,
            text: 'My Cart',
          ),
          GButton(
            icon: Icons.show_chart_outlined,
            text: 'My Order',
          ),
          GButton(
            icon: Icons.person,
            text: 'My Account',
          )
        ],
      ),
    );
  }
}
