import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';

AppBar odrinaryAppbar(String title, IconData iconName) {
  return AppBar(
    backgroundColor: primaryColor,
    title: Text(title),
    actions: [Icon(iconName)],
  );
}

AppBar appBarBack(BuildContext context, String title, Function() backNav) {
  return AppBar(
    backgroundColor: primaryColor,
    title: Text(title),
    centerTitle: true,
    leading: IconButton(
      onPressed: backNav,
      icon: Icon(Icons.arrow_back),
    ),
  );
}

Container textFieldForForm(String hintText, TextInputType textInputType,
    IconData iconData, Function(String) callback) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
        ]),
    child: TextFormField(
      onChanged: callback,
      keyboardType: textInputType,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter correct ${hintText}';
        }
        return null;
      },
      style: TextStyle(
        color: Colors.black87,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.only(top: 14),
        prefixIcon: Icon(
          iconData,
          color: Colors.grey,
        ),
        hintText: hintText,
        hintStyle: TextStyle(color: Colors.black38),
      ),
    ),
  );
}

textFieldLabel(String title) {
  return Align(
    alignment: Alignment.topLeft,
    child: Text(
      title,
      style: TextStyle(
          color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
    ),
  );
}

//================================================================================================================================================================
// if cart is empty or order is empty
itemsEmpty(String title) {
  return Container(
    child: Column(
      children: [
        Image.asset(
          "assets/c.png",
          width: 100,
        ),
        SizedBox(
          height: 16,
        ),
        Text(
          title,
          style: TextStyle(fontSize: 18),
        ),
      ],
    ),
  );
}
//================================================================================================================================================================

//==================================used in Checkout page==================================================================
Column checkoutTopIcon(String number, String text, Color checkColor) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Container(
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        decoration: BoxDecoration(
            color: checkColor, borderRadius: BorderRadius.circular(30)),
        child: Text(
          number,
          style: TextStyle(color: Colors.white, fontSize: 14),
        ),
      ),
      SizedBox(
        height: 3,
      ),
      Text(
        text,
        style: TextStyle(fontSize: 16),
      ),
    ],
  );
}
//=================================================================

//+++==========================================================alert dialog=======================================================================

AlertDialog alertDialogGlobal(BuildContext context, title, Function() yesFunc) {
  return AlertDialog(
    buttonPadding: EdgeInsets.all(8),
    title: Text(title),
    actions: [
      TextButton(
        style: TextButton.styleFrom(
            primary: primaryColor,
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10)),
        onPressed: () => Navigator.pop(context),
        child: Text(
          "No",
          style: TextStyle(color: primaryColor, fontSize: 18),
        ),
      ),
      TextButton(
        style: TextButton.styleFrom(
            backgroundColor: primaryColor,
            onSurface: primaryColor,
            primary: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10)),
        onPressed:           yesFunc,
        child: Text(
          "Yes",
          style: TextStyle(color: Colors.white, fontSize: 18),
        ),
      ),
    ],
  );
}
//======================================================================================================================
