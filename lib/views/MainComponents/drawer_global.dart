import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/categoryController.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/category.dart';
import 'package:frontend_ui/models/routesArgs.dart';
import 'package:frontend_ui/views/LoginScreen/login_screen.dart';
import 'package:frontend_ui/views/productsByCategory/products_by_category.dart';
import 'package:provider/provider.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({
    Key? key,
  }) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  void initState() {
    super.initState();
    final categoryItems =
        Provider.of<CategoryController>(context, listen: false);
    categoryItems.category(context);
    final checker = Provider.of<VerificationChecker>(context, listen: false);
    checker.isLoggedIn();
  }

  @override
  Widget build(BuildContext context) {
    final categoryItems =
        Provider.of<CategoryController>(context, listen: false);
    final cartController = Provider.of<CartController>(context, listen: false);
    final checker = Provider.of<VerificationChecker>(context, listen: false);

    buildSignOut() {
      return InkWell(
        onTap: () {
          checker.isloggetOut();
          cartController.cartItems = [];
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("Logged out Successfully"),
            ),
          );
          Navigator.pop(context);
        },
        child: ListTile(
          title: Text('Sign Out'),
          leading: Icon(Icons.logout, size: 25),
        ),
      );
    }

    buildSettingBtn() {
      return InkWell(
        onTap: () {
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => Settings(),
          //   ),
          // );
        },
        child: ListTile(
          title: Text('Setting'),
          leading: Icon(Icons.settings),
        ),
      );
    }

    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Material(
            color: primaryColor,
            child: DrawerHeader(
              child: Row(
                children: [
                  Container(
                    height: 90,
                    width: 90,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(width: 2, color: Colors.white),
                      image: DecorationImage(
                        fit: BoxFit.contain,
                        image: NetworkImage(url + "image/default.png"),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15),
                    child: checker.isloggedIn == true
                        ? Wrap(direction: Axis.vertical, children: [
                            FittedBox(
                              child: Text(
                                "User User",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            ),
                            Text(
                              "user1@gmail.com",
                              overflow: TextOverflow.ellipsis,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ])
                        : TextButton(
                            style: TextButton.styleFrom(
                              padding: EdgeInsets.symmetric(horizontal: 60),
                              primary: Colors.black,
                              backgroundColor: Colors.white,
                              elevation: 5,
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, LoginScreen.route);
                            },
                            child: Text(
                              "Login",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                            ),
                          ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            // flex: 4,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: categoryItems.categorylist.length,
              itemBuilder: (BuildContext context, int index) => ListCategory(
                listCategory: categoryItems.categorylist[index],
              ),
            ),
          ),
          checker.isloggedIn == true ? buildSignOut() : SizedBox(),
          buildSettingBtn()
        ],
      ),
    );
  }

  void isloggetOut() {}
}

class ListCategory extends StatelessWidget {
  ListCategory({Key? key, required this.listCategory}) : super(key: key);
  final CategoryList listCategory;

  @override
  Widget build(BuildContext context) {
    Widget _buildList(CategoryList listCategory) {
      if (listCategory.children.isEmpty) {
        return ListTile(
          onTap: () {
            Navigator.pushNamed(
              context,
              ProductsByCategory.route,
              arguments:
                  ProductsByCategoryArgs(listCategory.id, listCategory.name),
            );
          },
          title: Text(listCategory.name),
        );
      }
      return ExpansionTile(
        key: PageStorageKey<CategoryList>(listCategory),
        title: Text(listCategory.name.toUpperCase()),
        children: listCategory.children.map<Widget>(_buildList).toList(),
      );
    }

    return _buildList(listCategory);
  }
}
