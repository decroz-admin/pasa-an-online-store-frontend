import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/categoryController.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/Widgets/popular_products.dart';
import 'package:frontend_ui/models/routesArgs.dart';
import 'package:frontend_ui/views/HomeScreen/components/SliverAppBar.dart';
import 'package:frontend_ui/views/HomeScreen/components/category.dart';
import 'package:frontend_ui/views/HomeScreen/components/image_carousel.dart';
import 'package:frontend_ui/views/MainComponents/drawer_global.dart';
import 'package:frontend_ui/views/allProducts/all_Products.dart';
import 'package:frontend_ui/views/productsByCategory/products_by_category.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  static String route = 'home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    final categoryItems =
        Provider.of<CategoryController>(context, listen: false);
    categoryItems.category(context);
    final productController =
        Provider.of<ProductController>(context, listen: false);
    productController.recentlyAdded();
  }

  @override
  Widget build(BuildContext context) {
    final categoryItems = Provider.of<CategoryController>(context);
    final productController = Provider.of<ProductController>(context);
    return Scaffold(
      drawer: AppDrawer(),
      body: CustomScrollView(slivers: [
        HomeAppBar(),
        SliverList(
          delegate: SliverChildListDelegate([
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ImageCarausel(carauselHeight: 200.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: Text(
                    "Categories",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 125,
                  child: ListView.builder(
                      itemCount: categoryItems.categorylist.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) => InkWell(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, ProductsByCategory.route,
                                  arguments: ProductsByCategoryArgs(
                                      categoryItems.categorylist[index].id,
                                      categoryItems.categorylist[index].name));
                            },
                            child: CategoryWidget(
                              listCategory: categoryItems.categorylist[index],
                            ),
                          )),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [
                      Text(
                        "Popular Products",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontWeight: FontWeight.w800, fontSize: 20),
                      ),
                      Spacer(),
                      TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, AllProducts.route);
                          },
                          child: Text("View All >")),
                      // TextButton(onPressed: (){}, child: Row(
                      //   children: [
                      //     Text(data)
                      //   ],
                      // ))
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 254,
                  margin: EdgeInsets.symmetric(horizontal: 3),
                  // child: GridView.builder(
                  //   itemCount:8,
                  //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //         crossAxisCount: 2, ),
                  //     itemBuilder: (BuildContext ctx, int index) =>
                  //         PopularProducts()),
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 4,
                    itemBuilder: (BuildContext ctx, int index) =>
                        PopularProducts(
                            productAttrib:
                                productController.productsList[index]),
                  ),
                )
              ],
            ),
          ]),
        )
      ]),
    );
  }
}
