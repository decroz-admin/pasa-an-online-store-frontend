import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/category.dart';

class CategoryWidget extends StatelessWidget {
  CategoryWidget({Key? key, required this.listCategory})
      : super(key: key);
  final CategoryList listCategory;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              fit: BoxFit.fill,
              image: listCategory.categoryImage.length==0
                  ? NetworkImage(
                      "https://www.publicdomainpictures.net/pictures/320000/velka/background-image.png",
                    )
                  : NetworkImage(
                      (url + "image/" + listCategory.categoryImage.toString()),
                    ),
            ),
          ),
          // image: AssetImage("${categories[index]['categoryImagePath']}",

          margin: EdgeInsets.symmetric(horizontal: 5),
          width: 100,
          height: 100,
        ),
        Positioned(
          bottom: 0,
          left: 10,
          right: 10,
          child: Container(
            child: Text(
              listCategory.name.toString(),
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
