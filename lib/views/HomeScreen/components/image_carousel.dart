import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';

class ImageCarausel extends StatelessWidget {
  final carauselHeight;

  const ImageCarausel({Key? key, this.carauselHeight}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(8, 8, 8, 0),
      height: carauselHeight,
      child: Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('assets/1.png'),
          AssetImage('assets/2.png'),
        ],
        autoplay: true,
        dotIncreaseSize: 2.0,
        dotSize: 4.0,
        borderRadius: true,
        radius: Radius.circular(9.0),
        dotBgColor: Colors.transparent,
        animationCurve: Curves.bounceIn,
        animationDuration: Duration(microseconds: 1000),
      ),
    );
  }
}
