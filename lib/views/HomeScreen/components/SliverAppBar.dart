import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';

class HomeAppBar extends StatefulWidget {
  @override
  _HomeAppBarState createState() => _HomeAppBarState();
}

class _HomeAppBarState extends State<HomeAppBar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: primaryColor,
      pinned: true,
      floating: true,
      bottom: PreferredSize(
        
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 4),
          child: Material(
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      hintText: '  Search the entire store here',
                    ),
                    onChanged: (value) {},
                  ),
                ),
                InkWell(
                  child: Icon(Icons.search),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
      ),
      expandedHeight: 100.0,
      title: Text(
        "shoppingApp",
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.chat_bubble_outline,
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.notifications_outlined,
          ),
        ),
      ],
    );
  }
}
