// ignore: unused_import
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_khalti/flutter_khalti.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/routesArgs.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/checkoutScreen/components/reviewContent.dart';
import 'package:frontend_ui/views/checkoutScreen/components/reviewOrder.dart';
import 'package:frontend_ui/views/confirmOrder/confirmOrder.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

class CheckoutReview extends StatelessWidget {
  const CheckoutReview({Key? key}) : super(key: key);
  static String route = "checkoutReview";

  Widget build(BuildContext context) {
    final cartController = Provider.of<CartController>(context);
    final checkoutController = Provider.of<CheckoutController>(context);
    final args =
        ModalRoute.of(context)!.settings.arguments as AddressAndPayment;
    String addId = args.addressId;
    String payType = args.paymentType;
    // print(payType);

    cartController.cartItems.forEach((element) {
      for (int i = 0; i < cartController.cartItems.length; i++) {}
    });
    // print(args.paymentType) ;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text("Checkout"),
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(LineIcons.arrowLeft),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(65),
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.all(12),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Spacer(),
                checkoutTopIcon("1", "Shipping", Colors.purple.shade400),
                SizedBox(
                  width: 10,
                ),
                checkoutTopIcon("2", "Payment", Colors.purple.shade400),
                SizedBox(
                  width: 10,
                ),
                checkoutTopIcon("3", "Review", Colors.purple.shade400),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: ReviewContent(addressId: addId),
      ),
      bottomNavigationBar:
          checkoutBottomAppBar(context, "Checkout", 105.0, LineIcons.check, () {
        showDialog(
          context: context,
          builder: (BuildContext context) => alertDialogGlobal(
            context,
            "Proceed to Checkout?",
            () async {
              if (payType != "Payment.COD") {
                _payViaKhalti() async {
                  FlutterKhalti _flutterKhalti = FlutterKhalti.configure(
                    publicKey:
                        "test_public_key_d10a910c30ac47b3b842dc83e4521bb9",
                    urlSchemeIOS: "KhaltiPayFlutterExampleScheme",
                    paymentPreferences: [
                      KhaltiPaymentPreference.KHALTI,
                    ],
                  );

                  KhaltiProduct product = KhaltiProduct(
                    id: cartController.cartItems[0].id,
                    amount: cartController.totalPrice.toDouble(),
                    name: cartController.cartItems[0].name,
                    // customData: {
                    //    "proId":cartController.cartItems[0].name,
                    //    "prodname":cartController.cartItems[0].name,
                    // }
                  );
                  _flutterKhalti.startPayment(
                    product: product,
                    onSuccess: (data) async {
                      // print("here is khalti data");
                      // print(data);
                      final result = await checkoutController.addOrderItems(
                          addId,
                          cartController.totalPrice,
                          data['token'],
                          cartController.cartItems[0].id,
                          cartController.cartItems[0].price,
                          cartController.cartItems[0].qty,
                          "khalti",
                          "completed",
                          checkoutController.dateDelivery);
                      // print("this i sdummy");
                      // print(result);

                      ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(content: Text("Successful")));
                      Navigator.pushNamed(
                        context,
                        ConfirmedOrder.route,
                      );
                                            cartController.cartItems = [];

                    },
                    onFaliure: (error) {
                      // print("Here is khalti error");
                      // print(error);
                      ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(content: Text("failed")));
                    },
                  );
                }

                _payViaKhalti();
              } else {
                final result = await checkoutController.addOrderItems(
                    addId,
                    cartController.totalPrice,
                    "Empty",
                    cartController.cartItems[0].id,
                    cartController.cartItems[0].price,
                    cartController.cartItems[0].qty,
                    "cod",
                    "pending",
                    checkoutController.dateDelivery);
                cartController.cartItems = [];

                Navigator.pushNamed(context, ConfirmedOrder.route);
              }
            },
          ),
        );
      }),
    );
  }
}
