import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/routesArgs.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/checkoutScreen/checkoutReview.dart';
import 'package:frontend_ui/views/checkoutScreen/components/paymentContent.dart';
import 'package:frontend_ui/views/checkoutScreen/components/reviewOrder.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

class CheckoutPayment extends StatelessWidget {
  const CheckoutPayment({Key? key}) : super(key: key);
  static String route = "checkoutPayment";
  Widget build(BuildContext context) {
    final checkoutController = Provider.of<CheckoutController>(context);
    final addressId = ModalRoute.of(context)!.settings.arguments.toString();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text("Checkout"),
        leading: InkWell(onTap:(){
          Navigator.pop(context);
        },child: Icon(LineIcons.arrowLeft),),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(65),
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.all(12),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Spacer(),
                checkoutTopIcon("1", "Shipping", Colors.purple),
                SizedBox(
                  width: 10,
                ),
                checkoutTopIcon("2", "Payment", Colors.purple),
                SizedBox(
                  width: 10,
                ),
                checkoutTopIcon("3", "Review", Colors.grey),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: PaymentContent(),
      ),
      bottomNavigationBar: checkoutBottomAppBar(
          context, "Review order", 90.0, LineIcons.angleRight, () {
        Navigator.pushNamed(context, CheckoutReview.route,
            arguments: AddressAndPayment(addressId, checkoutController.payment.toString()));
      }),
    );
  }
}
