import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/checkoutScreen/components/checkoutPayment.dart';
import 'package:frontend_ui/views/checkoutScreen/components/shippingContent.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

class CheckoutShipping extends StatefulWidget {
  const CheckoutShipping({Key? key}) : super(key: key);
  static String route = "checkoutShipping";

  @override
  _CheckoutShippingState createState() => _CheckoutShippingState();
}

class _CheckoutShippingState extends State<CheckoutShipping> {
  @override
  void initState() {
    final addressController =
        Provider.of<AddressController>(context, listen: false);
    addressController.fetchUserAddress();
    super.initState();
  }

  Widget build(BuildContext context) {
    final addressController =
        Provider.of<AddressController>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text("Checkout"),
        leading: InkWell(onTap:(){
          Navigator.pop(context);
        },child: Icon(LineIcons.arrowLeft),),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(65),
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.all(12),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Spacer(),
                checkoutTopIcon("1", "Shipping", Colors.purple),
                SizedBox(
                  width: 10,
                ),
                checkoutTopIcon("2", "Payment", Colors.grey),
                SizedBox(
                  width: 10,
                ),
                checkoutTopIcon("3", "Review", Colors.grey),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: ShippingContent(addressController: addressController),
      ),
      bottomNavigationBar: CheckOutPayment(),
    );
  }
}
