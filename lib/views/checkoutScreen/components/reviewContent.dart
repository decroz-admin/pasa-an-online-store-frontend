import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:frontend_ui/Widgets/date_picker.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/checkoutScreen/components/checkoutCartItems.dart';
import 'package:provider/provider.dart';

class ReviewContent extends StatelessWidget {
  const ReviewContent({Key? key, required this.addressId}) : super(key: key);
  final String addressId;

  @override
  Widget build(BuildContext context) {
    final checkoutController = Provider.of<CheckoutController>(context);
    final addressController = Provider.of<AddressController>(context);
    final cartController = Provider.of<CartController>(context);
    addressController.fetchAddress(addressId);
    final addressAttr = addressController.singleuserAddress[0];

    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(8),
          alignment: Alignment.bottomLeft,
          child: Text(
            "Order Review",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Divider(
            height: 4,
            thickness: 1,
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          alignment: Alignment.bottomLeft,
          child: Text(
            "Shipping Address",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          alignment: Alignment.bottomLeft,
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                    text: addressAttr.name + ", " + addressAttr.address + "\n",
                    style: TextStyle(color: Colors.black87, fontSize: 15)),
                TextSpan(
                    text: addressAttr.district +
                        ", " +
                        addressAttr.province +
                        "\n",
                    style: TextStyle(color: Colors.black87, fontSize: 15)),
                TextSpan(
                    text: addressAttr.mobileNumber +
                        ", " +
                        addressAttr.alternatePhone.toString(),
                    style: TextStyle(color: Colors.black87, fontSize: 15))
              ],
            ),
          ),
        ),
       
        Container(
          padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
          alignment: Alignment.bottomLeft,
          child: DeliveryDate()
        ),

        Container(
          padding: EdgeInsets.all(8),
          alignment: Alignment.bottomLeft,
          child: Text(
            "Order Summary",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8),
          alignment: Alignment.bottomLeft,
          child: cartItemsDropDown(
              checkoutController, 18.0, FontWeight.w500, Colors.black87),
        ),
        cartItems(context, checkoutController),
        Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
            alignment: Alignment.bottomLeft,
            child: Wrap(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "SubTotal:",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    Text(
                      "Rs. "+cartController.totalPrice.toString(),
                      style: TextStyle(fontSize: 16, color: primaryColor),
                    )
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Total:",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      "Rs. "+cartController.totalPrice.toString(),
                      style: TextStyle(
                          fontSize: 20,
                          color: primaryColor,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                )
              ],
            )),
      ],
    );
  }
}
