import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/models/userAddresses.dart';
import 'package:provider/provider.dart';

ListView addressList(BuildContext context, List<Address> addresses) {

  final addressController = Provider.of<AddressController>(context);
  return ListView.builder(
    shrinkWrap: true,
    itemCount: addresses.length,
    itemBuilder: (BuildContext context, int index) => ListTile(
      leading: Radio(
        value: addresses[index].id,
        groupValue: addressController.currentId,
        onChanged: (value) {
          addressController.currentId = value.toString();
          addressController.changeAddress(value.toString());
        },
      ),
      title: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: addresses[index].name.toString() +
                  ", " +
                  addresses[index].address.toString() +
                  "\n",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                  fontWeight: FontWeight.w500),
            ),
            TextSpan(
                text: addresses[index].district.toString() +
                    ", " +
                    addresses[index].province.toString() +
                    ", " +
                    addresses[index].zipCode.toString() +
                    "\n",
                style: TextStyle(color: Colors.black87, fontSize: 15)),
            TextSpan(
                text: addresses[index].mobileNumber.toString() +
                    ", " +
                    addresses[index].alternatePhone.toString() +
                    "\n",
                style: TextStyle(color: Colors.black87, fontSize: 15))
          ],
        ),
      ),
    ),
  );
}
