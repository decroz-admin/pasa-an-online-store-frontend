import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:line_icons/line_icons.dart';

BottomAppBar checkoutBottomAppBar(
    BuildContext context, buttonTitle, hortzPad, iconName, Function() func) {
  return BottomAppBar(
    child: Container(
      height: 120,
      width: double.infinity,
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  LineIcons.arrowLeft,
                  size: 21,
                  color: primaryColor,
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                  child: Text(
                    "Previous",
                    style: TextStyle(
                        fontSize: 21,
                        color: primaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 18,
          ),
          GestureDetector(
            onTap: func,
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  child: Icon(
                    iconName,
                    color: Colors.white,
                  ),
                  color: Color(0xFF390c4d).withOpacity(0.8),
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: hortzPad, vertical: 8),
                  child: Center(
                    child: Text(
                      buttonTitle,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  color: primaryColor,
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
