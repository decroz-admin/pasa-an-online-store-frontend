import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/checkoutScreen/checkoutPayment.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

class CheckOutPayment extends StatelessWidget {
  const CheckOutPayment({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final addressController = Provider.of<AddressController>(context);
    return BottomAppBar(
      child: Container(
        height: 78,
        width: double.infinity,
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(
                  context,
                  CheckoutPayment.route,
                  arguments: addressController.currentId,
                );
              },
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    child: Icon(
                      LineIcons.angleRight,
                      color: Colors.white,
                    ),
                    color: Color(0xFF390c4d).withOpacity(0.8),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 62, vertical: 8),
                    child: Center(
                      child: Text(
                        "Proceed to Payment",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                    color: primaryColor,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
