import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

cartItems(BuildContext context, CheckoutController checkoutController) {
  final cartController = Provider.of<CartController>(context);
  return checkoutController.showCartItems
      ? SizedBox()
      : Container(
          margin: EdgeInsets.only(bottom: 5),
          child: ListView.builder(
            itemCount: cartController.cartItems.length,
            shrinkWrap: true,
            itemBuilder: (BuildContext ctx, int index) {
              return ListTile(
                leading: Image.network(
                  url + "image/" + cartController.cartItems[index].imag,
                  width: 70,
                  fit: BoxFit.cover,
                ),
                title: Text(
                  cartController.cartItems[index].name,
                ),
                subtitle: Text(
                  cartController.cartItems[index].qty.toString(),
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black87),
                ),
                trailing: Container(
                  width: 80,
                  child: Text(
                    "Rs. " +
                        (cartController.cartItems[index].qty *
                                cartController.cartItems[index].price)
                            .toString(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 18),
                  ),
                ),
              );
            },
          ),
        );
}

cartItemsDropDown(
    CheckoutController checkoutController, fontsize, fontweight, color) {
  return TextButton(
    onPressed: () {
      checkoutController.showList();
    },
    child: Row(
      children: [
        Text(
          "Cart Items()",
          style: TextStyle(
              color: color, fontSize: fontsize, fontWeight: fontweight),
        ),
        Spacer(),
        Icon(
          LineIcons.angleDown,
          color: color,
          size: fontsize,
        )
      ],
    ),
  );
}
