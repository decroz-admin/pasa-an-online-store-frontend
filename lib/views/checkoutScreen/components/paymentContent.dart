import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:provider/provider.dart';

class PaymentContent extends StatelessWidget {
  const PaymentContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final checkoutController = Provider.of<CheckoutController>(context);

    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(8),
          alignment: Alignment.bottomLeft,
          child: Text(
            "Payment Method",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        ListTile(
          title: const Text('Cash on Delivery'),
          leading: Radio(
            value: Payment.values[0],
            groupValue: checkoutController.payment,
            onChanged: (value) {
              checkoutController.paymentType(value);
            },
          ),
        ),
        ListTile(
          title: const Text('Khalti'),
          leading: Radio(
            value: Payment.values[1],
            groupValue: checkoutController.payment,
            onChanged: (Payment? value) {
              checkoutController.paymentType(value);
            },
          ),
        ),
      ],
    );
  }
}
