import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/addAddress/addAddress.dart';
import 'package:frontend_ui/views/checkoutScreen/components/checkoutCartItems.dart';
import 'package:provider/provider.dart';

import 'addressList.dart';

class ShippingContent extends StatelessWidget {
  const ShippingContent({Key? key, required this.addressController})
      : super(key: key);
  final AddressController addressController;

  @override
  Widget build(BuildContext context) {
    final checkoutController = Provider.of<CheckoutController>(context);
    var addresses = addressController.userAddress.address;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: 5),
                child: Row(
                  children: [
                    Text(
                      "Shipping Address",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    TextButton(
                      onPressed: () {
                        Navigator.popAndPushNamed(context, AddAddress.route, arguments: "fromCheckOut");
                      },
                      child: Text(
                        "Add New",
                        style: TextStyle(color: primaryColor),
                      ),
                    ),
                  ],
                ),
              ),
              addressList(context, addresses),
              cartItemsDropDown(
                  checkoutController, 20.0, FontWeight.bold, Colors.black),
              cartItems(context, checkoutController),
            ],
          ),
        ),
      ],
    );
  }
}