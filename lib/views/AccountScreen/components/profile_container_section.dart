import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/constraints.dart';

class ProfileContainerSection extends StatelessWidget {
  const ProfileContainerSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool loggedIn = false;
    return Material(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Row(
          children: [
            Image.network(
              url + "image/default.png",
              width: 80,
              height: 100,
              fit: BoxFit.fill,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 12, left: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(Icons.portrait),
                      Padding(
                        padding: EdgeInsets.all(4),
                      ),
                      Text(
                        "Sagar Rajbhandari",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: InkWell(
                    onTap: () {
                      // print("edit profile clicked");
                    },
                    child: Text(
                      "Edit Profile",
                      style: TextStyle(
                        color: primaryColor,
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
