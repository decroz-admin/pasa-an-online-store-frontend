import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/views/AccountScreen/components/profile_container_section.dart';
import 'package:frontend_ui/views/MainComponents/drawer_global.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/NotLoggedInScreen/not_logged_in_screen.dart';
import 'package:frontend_ui/views/savedAddress/savedAddress.dart';
import 'package:provider/provider.dart';

class AccountScreen extends StatelessWidget {
  AccountScreen({Key? key}) : super(key: key);
  static String route = "account";

  @override
  Widget build(BuildContext context) {
    final checker = Provider.of<VerificationChecker>(context);
    checker.isLoggedIn();

    return Scaffold(
      appBar: odrinaryAppbar("My Account", Icons.settings),
      drawer: AppDrawer(),
      body: Center(
        child: SingleChildScrollView(
          child: checker.isloggedIn == true
              ? Column(
                  children: [
                    ProfileContainerSection(),
                    SizedBox(
                      height: 8,
                    ),
                    accountListTie('My Orders', "Check your Order Status", 0,
                        context, () {}),
                    accountListTie(
                        'My Returns', "Request for retuens", 1, context, () {}),
                    accountListTie(
                        'Saved Address', "Change/add address", 2, context, () {
                      Navigator.pushNamed(context, SavedAddress.route);
                    }),
                    Divider(),
                    accountListTie(
                        'Invite Friends',
                        "Share and Invite friends and family",
                        3,
                        context,
                        () {}),
                    accountListTie(
                        'Help Center',
                        "Contact help senter for any queries",
                        4,
                        context,
                        () {}),
                    accountListTie('Rate This App',
                        "Rate this app and show soe love", 5, context, () {}),
                  ],
                )
              : NotLoggedInScreen(),
        ),
      ),
    );
  }

  List<IconData> _accountListTie = [
    Icons.three_k_outlined,
    Icons.restore_rounded,
    Icons.house_outlined,
    Icons.email_outlined,
    Icons.help_center_outlined,
    Icons.star_border_outlined,
  ];

  Widget accountListTie(String title, String subTitle, int index,
      BuildContext context, Function() tapFunc) {
    return ListTile(
      onTap: tapFunc,
      title: Text(title),
      subtitle: Text(subTitle),
      leading: Icon(_accountListTie[index]),
      trailing: Icon(Icons.arrow_forward),
    );
  }
}
