import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/savedAddress/savedAddress.dart';
import 'package:line_icons/line_icons.dart';
import 'package:frontend_ui/services/apiServices.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/checkoutScreen/checkoutShipping.dart';

class AddAddress extends StatelessWidget {
  const AddAddress({Key? key}) : super(key: key);

  static String route = "addAddress";
  @override
  Widget build(BuildContext context) {
    String args = ModalRoute.of(context)!.settings.arguments as String;
    // print(args);
    final _addressFormKey = GlobalKey<FormState>();

    String name = "";
    String mobile = "";
    String alterMobile = "";
    String province = "";
    String zipCode = '';
    String district = '';
    String streetAdd = "";

    return Scaffold(
      appBar: appBarBack(
        context,
        "Add Address",
        () {
          Navigator.pop(context);
        },
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _addressFormKey,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                textFieldLabel("Full Name"),
                SizedBox(
                  height: 10,
                ),
                textFieldForForm("Name", TextInputType.name, LineIcons.user,
                    (value) {
                  name = value;
                }),
                SizedBox(
                  height: 10,
                ),
                textFieldLabel("Province"),
                SizedBox(
                  height: 10,
                ),
                textFieldForForm("Province", TextInputType.name, LineIcons.home,
                    (value) {
                  province = value;
                }),
                SizedBox(
                  height: 10,
                ),
                textFieldLabel("District"),
                SizedBox(
                  height: 10,
                ),
                textFieldForForm(
                    "District", TextInputType.name, LineIcons.building,
                    (value) {
                  district = value;
                }),
                SizedBox(
                  height: 10,
                ),
                textFieldLabel("Street/Tole"),
                SizedBox(
                  height: 10,
                ),
                textFieldForForm(
                    "Tole", TextInputType.text, LineIcons.streetView, (value) {
                  streetAdd = value;
                }),
                SizedBox(
                  height: 10,
                ),
                textFieldLabel("Zip Code"),
                SizedBox(
                  height: 10,
                ),
                textFieldForForm("code", TextInputType.number,
                    LineIcons.unitedStatesPostalService, (value) {
                  zipCode = value;
                }),
                SizedBox(
                  height: 10,
                ),
                textFieldLabel("Mobile Number"),
                SizedBox(
                  height: 10,
                ),
                textFieldForForm(
                    "Mobile", TextInputType.number, LineIcons.mobilePhone,
                    (value) {
                  mobile = value;
                }),
                SizedBox(
                  height: 10,
                ),
                textFieldLabel("Alternate Mobile"),
                SizedBox(
                  height: 10,
                ),
                textFieldForForm(
                  "Alternate",
                  TextInputType.number,
                  LineIcons.alternateMobile,
                  (value) {
                    alterMobile = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextButton(
                  onPressed: () async {
                    if (_addressFormKey.currentState!.validate()) {
                      String addressType = "home";
                      String result = await addAddresses(
                          name,
                          zipCode,
                          streetAdd,
                          province,
                          district,
                          mobile,
                          alterMobile,
                          addressType);
                      if (result == "Added Successful") {
                        // naviagate
                        if (args == 'fromCheckOut') {
                          Navigator.pushNamed(
                              context, CheckoutShipping.route);
                        } else {
                        Navigator.pushNamed(context, SavedAddress.route);
                        }
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                              content:
                                  Text("Delivery address added successful")),
                        );
                      } else {
                        //show error
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text("Failed to add new address")),
                        );
                      }
                    }
                  },
                  style: TextButton.styleFrom(
                    minimumSize: Size(double.infinity, 15),
                    primary: Colors.black,
                    backgroundColor: primaryColor,
                    elevation: 5,
                  ),
                  child: Text(
                    "Add Address",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
