import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/addReview.dart';

class NoReview extends StatefulWidget {
  const NoReview({Key? key, this.productAttributes}) : super(key: key);
  final productAttributes;

  @override
  _NoReviewState createState() => _NoReviewState();
}

class _NoReviewState extends State<NoReview> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Icon(
              Icons.error_outline,
              size: 40,
              color: primaryColor,
            ),
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                color: Colors.purple.shade100,
                borderRadius: BorderRadius.circular(30)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              "No Review Yet",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 16),
            width: double.infinity,
            child: TextButton(
              onPressed: () {
                Navigator.pushNamed(context, AddReview.route, arguments: widget.productAttributes.id);
              },
              style: TextButton.styleFrom(
                primary: Colors.black,
                backgroundColor: primaryColor,
                elevation: 5,
              ),
              child: Text(
                "Write a Review",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 18),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
