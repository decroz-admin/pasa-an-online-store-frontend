import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/services/apiServices.dart';
import 'package:frontend_ui/views/CartScreen/cart.dart';
import 'package:provider/provider.dart';

class SecondaryBottomNav extends StatefulWidget {
  final String productId;
  // final int quantity;

  SecondaryBottomNav({required this.productId});
  @override
  _SecondaryBottomNavState createState() => _SecondaryBottomNavState();
}

class _SecondaryBottomNavState extends State<SecondaryBottomNav> {
  @override
  void initState() {
    final cartController = Provider.of<CartController>(context, listen: false);
    cartController.addItemsToCart(
      widget.productId,
      cartController.counterValue,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cartController = Provider.of<CartController>(context);
    final verificationChecker = Provider.of<VerificationChecker>(context);
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          MaterialButton(
            padding: EdgeInsets.all(0.0),
            minWidth: 145,
            onPressed: () async {
              verificationChecker.isLoggedIn();
              // print(verificationChecker.isloggedIn);
              final result = await addItemToCart(
                    widget.productId, cartController.counterValue);
              if (verificationChecker.isloggedIn == true) {
                final result = await addItemToCart(
                    widget.productId, cartController.counterValue);
                if (result == "Items Successfully added to cart") {
                  Navigator.pushNamed(context, CartScreen.route);
                  cartController.fetchPoductImage(context);
                } else {
                  SnackBar(
                    content: Text("Failed to add"),
                  );
                }
              } else {
                Navigator.pushNamed(context, CartScreen.route);
              }
            },
            color: primaryColor,
            child: Row(
              children: [
                Container(
                  width: 30,
                  color: Color(0xFF390c4d),
                  child: Icon(
                    Icons.add_shopping_cart_outlined,
                    color: Colors.white,
                  ),
                ),
                Container(
                  width: 105,
                  child: Text(
                    "Add to Cart",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
          MaterialButton(
            padding: EdgeInsets.all(0.0),
            minWidth: 145,
            onPressed: () {},
            color: Colors.greenAccent.shade700,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: EdgeInsets.all(0.0),
                  width: 30,
                  color: Color(0xFF094f49),
                  child: Icon(
                    Icons.flash_on,
                    color: Colors.white,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(0.0),
                  width: 105,
                  child: Text(
                    'Buy Now',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            color: primaryColor,
            onPressed: () {},
            icon: Icon(Icons.share),
          ),
        ],
      ),
    );
  }
}
