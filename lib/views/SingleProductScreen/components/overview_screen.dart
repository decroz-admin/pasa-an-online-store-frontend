import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/Widgets/popular_products.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/SingleProductScreen/single_product_screen.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

class OverviewScreen extends StatefulWidget {
  final productAttributes;
  // final Function cartQtyFunc;
  // const OverviewScreen(
  //     {Key? key, required this.productAttributes, this.cartQtyFunc})
  //     : super(key: key);
  OverviewScreen({
    this.productAttributes,
  });
  @override
  _OverviewScreenState createState() => _OverviewScreenState();
}

class _OverviewScreenState extends State<OverviewScreen> {
  @override
  void initState() {
    final productsItem =
        Provider.of<ProductByCategoriesController>(context, listen: false);
    productsItem.findProductsById(context, widget.productAttributes.id);
    print(widget.productAttributes.idx);
    productsItem.fetchRecommended(context, widget.productAttributes.idx);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final productsItem = Provider.of<ProductByCategoriesController>(context);
    final cartController = Provider.of<CartController>(context);

    print(productsItem.recommendedProducts);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // ImageCarausel(
          //   carauselHeight: 300.0,
          // ),
          // ==========================for product image=============================================
          Container(
            height: 300,
            alignment: Alignment.center,
            child: Image.network(
              (url + "image/" + productsItem.proById[0].productImages[0].img)
                  .toString(),
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    productsItem.proById[0].name.toString(),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        color: Colors.black),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "\Rs ${productsItem.proById[0].price}",
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ),
                        Spacer(
                          flex: 2,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: primaryColor,
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 4.0),
                          child: Text(
                            "12.36 " + "% off",
                            style: TextStyle(
                              color: primaryColor,
                            ),
                          ),
                        ),
                        SizedBox(width: 20),
                        Text(
                          "\Rs. 6000",
                          style: TextStyle(
                            decoration: TextDecoration.lineThrough,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 1),
                  Divider(
                    height: 2,
                    color: Colors.black,
                  ),
                  SizedBox(height: 10),
                  //Size swatch starts here
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Sizes ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: Colors.black),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Wrap(
                          children: List.generate(4, (index) {
                            return GestureDetector(
                              onTap: () {
                                // setState(() {
                                //   activeSize =
                                //       widget.productDetails.size[index];
                                // });
                              },
                              child: Container(
                                margin: EdgeInsets.only(right: 15),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 12),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    width: 2,
                                    color: Colors.transparent,
                                  ),
                                  color: Colors.white,
                                ),
                                child: Text(
                                  "S",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.black),
                                ),
                              ),
                            );
                          }),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  //Color swatch starts here
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Colors ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: Colors.black),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Wrap(
                          children: List.generate(4, (index) {
                            return GestureDetector(
                              onTap: () {
                                // setState(() {
                                //   activeColor = widget
                                //       .productDetails.color[index];
                                //   activeImg = widget
                                //       .productDetails.color[index];
                                // });
                              },
                              child: Container(
                                width: 45,
                                height: 50,
                                margin: EdgeInsets.only(right: 15),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 8),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage("assets/2.png"),
                                      fit: BoxFit.cover),
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    width: 3,
                                    color: Colors.transparent,
                                  ),
                                  color: Colors.black,
                                ),
                              ),
                            );
                          }),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  //Quantiy options starts here
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Qty",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: Colors.black),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Flexible(
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                // print(productsItem.proById[0].quantity);
                                // print("ghatako" + quantity.toString());
                                // if (quantity > 1 &&
                                //     quantity <=
                                //         productsItem.proById[0].quantity) {
                                //   setState(() {
                                //     quantity = quantity - 1;
                                //     widget.cartQtyFunc(quantity);
                                //   });
                                // }
                                if (cartController.counterValue > 1) {
                                  cartController.decreaseQty();
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  border:
                                      Border.all(color: Colors.black, width: 2),
                                ),
                                width: 25,
                                height: 25,
                                child: Icon(
                                  LineIcons.minus,
                                  color: Colors.black,
                                  size: 15,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Text(
                              cartController.counterValue.toString(),
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            GestureDetector(
                              onTap: () {
                                // print("badako" + quantity.toString());
                                // if (quantity <
                                //     productsItem.proById[0].quantity) {
                                //   setState(() {
                                //     quantity = quantity + 1;
                                //     widget.cartQtyFunc(quantity);
                                //   });
                                // }
                                if (cartController.counterValue <
                                    productsItem.proById[0].quantity) {
                                  cartController.increaseQty();
                                }
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    border: Border.all(
                                        color: Colors.black, width: 2),
                                  ),
                                  width: 25,
                                  height: 25,
                                  child: Icon(
                                    LineIcons.plus,
                                    color: Colors.black,
                                    size: 15,
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Text(
              "You may also like",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700),
            ),
          ),
          Container(
            width: double.infinity,
            height: 254,
            margin: EdgeInsets.symmetric(horizontal: 3),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: productsItem.recommendedProducts.length,
              itemBuilder: (BuildContext context, int index) => PopularProducts(
                  productAttrib: productsItem.recommendedProducts[index]),
            ),
          ),
        ],
      ),
    );
  }
}
