import 'package:flutter/material.dart';

class DetailsScreen extends StatefulWidget {
  final productAttributes;

  DetailsScreen({
    this.productAttributes,
  });

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  @override
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              "Product Details",
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                  color: Colors.black),
            ),
          ),
          Divider(
            height: 5,
            color: Colors.black,
          ),
          SizedBox(height: 10),
          Text(
            widget.productAttributes.description,
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ],
      ),
    );
  }
}
