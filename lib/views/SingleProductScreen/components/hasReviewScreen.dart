import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/LoginScreen/login_screen.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/addReview.dart';
import 'package:provider/provider.dart';

class HasReview extends StatefulWidget {
  const HasReview({Key? key, this.productAttributes}) : super(key: key);
  final productAttributes;

  @override
  _HasReviewState createState() => _HasReviewState();
}

class _HasReviewState extends State<HasReview> {
  @override
  Widget build(BuildContext context) {
    final checker = Provider.of<VerificationChecker>(context);

    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.separated(
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) => Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          radius: 25,
                          backgroundImage: NetworkImage(
                              "https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg"
                              ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Name and Name",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w400),
                            ),
                            RatingBar.builder(
                              itemSize: 20,
                              initialRating: widget
                                  .productAttributes.reviews[index].rating
                                  .toDouble(),
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, initialRating) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              tapOnlyMode: true,
                              onRatingUpdate: (rating) {},
                            ),
                            Text(
                              widget.productAttributes.reviews[index].review
                                  .toString(),
                              textAlign: TextAlign.justify,
                              style:
                                  TextStyle(fontSize: 18, color: Colors.black),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
              separatorBuilder: (BuildContext context, int index) => Divider(
                    thickness: 2,
                  ),
              itemCount: widget.productAttributes.reviews.length),
          Container(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 16),
            width: double.infinity,
            child: TextButton(
              onPressed: () {
                checker.isLoggedIn();
// print(checker.isloggedIn);
                if (checker.isloggedIn == true) {
                  Navigator.pushNamed(context, AddReview.route,
                      arguments: widget.productAttributes.id);
                } else {
                  Navigator.pushNamed(context, LoginScreen.route,
                      arguments: widget.productAttributes.id);
                }
              },
              style: TextButton.styleFrom(
                primary: Colors.black,
                backgroundColor: primaryColor,
                elevation: 5,
              ),
              child: Text(
                "Add your Review",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 18),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
