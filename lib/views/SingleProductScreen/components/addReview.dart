import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/services/apiServices.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:provider/provider.dart';

class AddReview extends StatelessWidget {
  const AddReview({Key? key}) : super(key: key);

  static String route = "addReview";
  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context)!.settings.arguments as String;
    final productController = Provider.of<ProductController>(context);

    return Scaffold(
      appBar: appBarBack(context, "Add Review", () {
        Navigator.pop(context);
      }),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Your ratings",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Overall Ratings",
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  RatingBar.builder(
                    itemSize: 28,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 5.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      productController.rating(rating);
                    },
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                    color: Colors.green,
                    child: Wrap(
                      spacing: 8,
                      children: [
                        Text(productController.rate.toString(),
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        Icon(
                          Icons.star,
                          color: Colors.white,
                          size: 25,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Your Review",
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 1)),
                child: TextField(
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    productController.reviews(value);
                  },
                  maxLines: 5,
                  // style: TextStyle(color: Colors.black87),
                  decoration: InputDecoration(
                      hintText: " Review details",
                      border: InputBorder.none,
                      hintStyle:
                          TextStyle(fontSize: 16, color: Colors.black38)),
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: TextButton(
        onPressed: () async {
          // print("review");

          if (productController.review != "" ||
              productController.review.isNotEmpty) {
            final result = await addReviews(
                productController.rate, productController.review, id);
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Successful')),
            );
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Fields are required')),
            );
          }
        },
        child: Row(
          children: [
            Container(
                alignment: Alignment.center,
                height: 40,
                color: primaryColor,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Icon(
                  Icons.check_outlined,
                  color: Colors.white,
                  size: 25,
                )),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(vertical: 10),
                color: primaryColor,
                height: 40,
                child: Text(
                  "Submit",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
