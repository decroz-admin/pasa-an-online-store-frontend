import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/CartScreen/cart.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/bottomOptionBar.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/details_screen.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/hasReviewScreen.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/noReview.dart';
import 'package:frontend_ui/views/SingleProductScreen/components/overview_screen.dart';
import 'package:provider/provider.dart';

class SingleProductScreen extends StatefulWidget {
  static String route = "singleProductScreen";

  @override
  _SingleProductScreenState createState() => _SingleProductScreenState();
}

class _SingleProductScreenState extends State<SingleProductScreen> {
  // int quantity = 1;
  // CartQty(int quantity) {
  //   setState(() {
  //     this.quantity = quantity;
  //   });
  // }

  @override
  void initState() {
    super.initState();
    final productsItem =
        Provider.of<ProductByCategoriesController>(context, listen: false);
    Future.delayed(Duration.zero, () {
      setState(() {
        final args = ModalRoute.of(context)!.settings.arguments as String;
        //    print(args);
        productsItem.findProductsById(context, args.toString());
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final productsItem = Provider.of<ProductByCategoriesController>(context);
    //   print(productsItem.proById[0].id);
    String id = productsItem.proById[0].id;
    return Scaffold(
      body: SafeArea(
        child: DefaultTabController(
          length: 3,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  backgroundColor: primaryColor,
                  floating: false,
                  pinned: false,
                  title: Text(productsItem.proById[0].name),
                  actions: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.search_outlined),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pushNamed(context, CartScreen.route);
                      },
                      icon: Icon(Icons.shopping_cart),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.more_vert),
                    ),
                  ],
                ),
                SliverPersistentHeader(
                  delegate: _SliverAppBarDelegate(
                    TabBar(
                      labelColor: Colors.black87,
                      indicatorSize: TabBarIndicatorSize.label,
                      indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(width: 2.0)),
                      tabs: [
                        Tab(text: "Overview"),
                        Tab(text: "Details"),
                        Tab(text: "Review"),
                      ],
                    ),
                  ),
                  pinned: true,
                ),
              ];
            },
            body: TabBarView(
              children: [
                OverviewScreen(
                  productAttributes: productsItem.proById[0],
                ),
                DetailsScreen(
                  productAttributes: productsItem.proById[0],
                ),
                productsItem.proById[0].reviews.length == 0
                    ? NoReview(
                        productAttributes: productsItem.proById[0],
                      )
                    : HasReview(
                        productAttributes: productsItem.proById[0],
                      ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: SecondaryBottomNav(productId: id),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
