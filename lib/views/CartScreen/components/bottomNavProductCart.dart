import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/cart.dart';
import 'package:frontend_ui/views/checkoutScreen/checkoutShipping.dart';
import 'package:provider/provider.dart';

class BottomNavForProductCart extends StatelessWidget {
//   const BottomNavForProductCart({
//     Key? key,
//   }) : super(key: key);
  final List<CartItem> item;
  const BottomNavForProductCart(List<CartItem> this.item);

  Widget build(BuildContext context) {
    int totPrice=0;
  final cartController = Provider.of<CartController>(context, listen: false);
    for (var i = 0; i < item.length; i++) {
      totPrice += item[i].price;
    }
    cartController.totalPriceProdut(totPrice);
    return BottomAppBar(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.0),
          ),
        ),
        height: 120,
        width: double.maxFinite,
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "SubTotal :",
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  
                  cartController.totalPrice.toString(),
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: primaryColor),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, CheckoutShipping.route);
              },
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    child: Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    color: Color(0xFF390c4d).withOpacity(0.8),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 105, vertical: 8),
                    child: Center(
                      child: Text(
                        "Checkout",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                    color: primaryColor,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
