import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/navController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/cart.dart';
import 'package:frontend_ui/views/MainComponents/bottombar.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

class CartFull extends StatefulWidget {
  // const CartFull({Key? key}) : super(key: key1);
  final CartItem item;
  const CartFull(CartItem this.item);

  @override
  _CartFullState createState() => _CartFullState();
}

class _CartFullState extends State<CartFull> {
  @override
  // void initState() {
  //   final cartController = Provider.of<CartController>(context, listen: false);
  //   cartController.deleteItemsCart(widget.item.id);
  //   super.initState()
  // }

  Widget build(BuildContext context) {
    final cartController = Provider.of<CartController>(context, listen: false);
    final navController = Provider.of<NavController>(context);
    // print(widget.item);
    return Card(
      margin: EdgeInsets.all(8),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.network(
                (url + "image/" + widget.item.imag).toString(),
                height: 90,
                width: 90,
                fit: BoxFit.cover,
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.item.name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: cartController.currentValues[widget.item.id]
                                  .toString(),
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 22,
                              ),
                            ),
                            TextSpan(
                              text: ' x ',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                              ),
                            ),
                            TextSpan(
                              text: widget.item.price.toString(),
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 22,
                              ),
                            ),
                            WidgetSpan(
                              child: Align(
                                alignment: Alignment.topRight,
                                child: Text(
                                  "Rs. " + widget.item.price.toString(),
                                  style: TextStyle(
                                    color: primaryColor,
                                    fontSize: 22,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Wrap(
                spacing: 15,
                children: [
                  GestureDetector(
                    onTap: () {
                      cartController.decreaseQtybyId(widget.item.id);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: Colors.black, width: 2),
                      ),
                      width: 25,
                      height: 25,
                      child: Icon(
                        LineIcons.minus,
                        size: 15,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Text(
                    cartController.currentValues[widget.item.id].toString(),
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      cartController.increaseQtybyId(widget.item.id);
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.black, width: 2),
                        ),
                        width: 25,
                        height: 25,
                        child: Icon(
                          Icons.add,
                          size: 15,
                          color: Colors.black,
                        )),
                  ),
                ],
              ),
              VerticalDivider(
                thickness: 20,
                color: Colors.red,
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.black),
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (_) => alertDialogGlobal(
                      context,
                      "Remove this item from cart?",
                      () async {
                        await cartController.deleteItemsCart(widget.item.id);
                        // print("ashish");

                        if (cartController.removeItem == "Item Deleted") {
                          // cartController.
                          navController.currentIndex(1);
                          Navigator.pushNamedAndRemoveUntil(
                              context, BottomNavBar.route, (route) => false);
                          SnackBar(
                            content: Text("Item removed successful"),
                          );
                        } else {
                          SnackBar(
                            content: Text("Unable to delete"),
                          );
                        }
                      },
                    ),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.delete_outline,
                      size: 30,
                    ),
                    Text(
                      "Remove",
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
