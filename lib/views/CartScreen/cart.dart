import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/views/CartScreen/components/bottomNavProductCart.dart';
import 'package:frontend_ui/views/CartScreen/components/cart_full.dart';
import 'package:frontend_ui/views/MainComponents/drawer_global.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/NotLoggedInScreen/not_logged_in_screen.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);
  static String route = 'cart';

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  void initState() {
    final checker = Provider.of<VerificationChecker>(context, listen: false);
    checker.isLoggedIn();

    if (checker.isloggedIn == true) {
      final itemsInCart = Provider.of<CartController>(context, listen: false);
      itemsInCart.fetchCart(context);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final checker = Provider.of<VerificationChecker>(context);
    final itemsInCart = Provider.of<CartController>(context);
    //  String cartId = itemsInCart.cart.cartItems[0].id;
    return Scaffold(
      appBar: odrinaryAppbar("My Cart", Icons.search_outlined),
      drawer: AppDrawer(),
      body: checker.isloggedIn == true
          ? (itemsInCart.cartItems.length == 0
              ? Center(
                  child: SingleChildScrollView(
                    child:
                        itemsEmpty("No items have been added to the cart yet"),
                  ),
                )
              : ListView.builder(
                  itemCount: itemsInCart.cartItems.length,
                  itemBuilder: (BuildContext context, int index) => CartFull(
                    itemsInCart.cartItems[index],
                  ),
                ))
          : Center(
              child: SingleChildScrollView(
              child: NotLoggedInScreen(),
            )),
      bottomNavigationBar: checker.isloggedIn == true
          ? (itemsInCart.cartItems.length == 0
              ? null
              : BottomNavForProductCart(itemsInCart.cartItems))
          : null,
    );
  }
}
