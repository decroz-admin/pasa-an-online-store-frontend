import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/views/AccountScreen/account.dart';
import 'package:frontend_ui/views/LoginScreen/components/afterLoginCard.dart';
import 'package:frontend_ui/views/LoginScreen/components/login_form.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/SignUpScreen/signuo_screen.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  static String route = 'login';

  @override
  Widget build(BuildContext context) {
    final checker = Provider.of<VerificationChecker>(context, listen: false);
    checker.isLoggedIn();
    if (checker.isloggedIn == true) {
      Navigator.pushNamed(context, AccountScreen.route);
    }
    return Scaffold(
      appBar: appBarBack(context, "Sign In", () {
        Navigator.pop(context);
      }),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              elevation: 5,
              margin: EdgeInsets.fromLTRB(20, 15, 20, 10),
              child: Padding(
                padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.0, 5.0),
                            blurRadius: 6.0,
                          ),
                        ],
                      ),
                      child: CircleAvatar(
                        foregroundImage: AssetImage("assets/a.png"),
                        radius: 40,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    LoginForm(),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            AfterLoginCard(),
            SizedBox(
              height: 5,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 5.0), //(x,y)
                    blurRadius: 6.0,
                  ),
                ],
              ),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 25,
                child: IconButton(
                  onPressed: () {
                    // final BottomNavigationBar navigationBar = navBarGlobalKey.currentWidget;
                    // initialIndex = 0;
                    // navigationBar.onTap(3);
                    Navigator.pushNamed(context, SignUpScreen.route);
                  },
                  icon: Icon(
                    Icons.email_outlined,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                "Email",
                style: TextStyle(fontSize: 18),
              ),
            )
          ],
        ),
      ),
    );
  }
}
