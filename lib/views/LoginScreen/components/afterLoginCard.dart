
import 'package:flutter/material.dart';

class AfterLoginCard extends StatelessWidget {
  const AfterLoginCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: new Container(
            margin: const EdgeInsets.only(left: 10.0, right: 15.0),
            child: Divider(
              color: Colors.black,
              height: 10,
              thickness: 1,
            ),
          ),
        ),
        Text(
          'New Customer? \n Sign Up with',
          style: TextStyle(fontSize: 18),
        ),
        Expanded(
          child: new Container(
            margin: const EdgeInsets.only(left: 15.0, right: 10.0),
            child: Divider(
              color: Colors.black,
              height: 10,
              thickness: 1,
            ),
          ),
        ),
      ],
    );
  }
}
