import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/services/apiServices.dart';
import 'package:frontend_ui/views/AccountScreen/account.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({
    Key? key,
  }) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String email = "";
  String password = "";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    return Form(
      key: _formKey,
      child: Column(
        children: [
          textFieldLabel("Email"),
          SizedBox(
            height: 10,
          ),
          textFieldForForm(
              "Email", TextInputType.emailAddress, Icons.email_outlined,
              (value) {
            email = value;
          }),
          SizedBox(height: 15),
          textFieldLabel("Password"),
          SizedBox(
            height: 10,
          ),
          textFieldForForm(
              "password", TextInputType.visiblePassword, Icons.lock, (value) {
            password = value;
          }),
          SizedBox(
            height: 10,
          ),
          TextButton(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                String result = await userLogin(email, password);
                if (result == "login Success") {
                  // naviagate
                  Navigator.pop(context);
                } else {
                  //show error
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text(result.toString())),
                  );
                }
                // ScaffoldMessenger.of(context).showSnackBar(
                //   const SnackBar(content: Text('Processing Data')),
                // );
              }
            },
            style: TextButton.styleFrom(
              minimumSize: Size(double.infinity, 15),
              primary: Colors.black,
              backgroundColor: primaryColor,
              elevation: 5,
            ),
            child: Text(
              "Log in",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 18),
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
