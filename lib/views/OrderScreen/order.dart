import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/orderController.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/services/apiServices.dart';
import 'package:frontend_ui/views/MainComponents/drawer_global.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/NotLoggedInScreen/not_logged_in_screen.dart';
import 'package:provider/provider.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  @override
  void initState() {
    final checker = Provider.of<VerificationChecker>(context, listen: false);
    checker.isLoggedIn();
accessToken();
    final orderController =
        Provider.of<OrderController>(context, listen: false);
    if (checker.isloggedIn == true) {
      final orderController =
          Provider.of<OrderController>(context, listen: false);
      orderController.fetchOrder();

      // if (orderController.custOrder.length != 0) {
      // }
    }
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final checker = Provider.of<VerificationChecker>(context, listen: false);

    final orderController =
        Provider.of<OrderController>(context, listen: false);

    // print(orderController.custOrder[0].id)
    print("number of order");
    print(orderController.custOrder.length);
    return Scaffold(
      appBar: odrinaryAppbar("My Orders", Icons.search_outlined),
      drawer: AppDrawer(),
      body: checker.isloggedIn == true
          ? (orderController.custOrder.length == 0
              ? Center(
                  child: SingleChildScrollView(
                    child: itemsEmpty("No items ordered yet!"),
                  ),
                )
              : ListView.builder(
                  shrinkWrap: true,
                  itemCount: orderController.custOrder.length,
                  itemBuilder: (BuildContext context, int index) => SingleOrder(orderController: orderController, index: index),
                ))
          : Center(
              child: SingleChildScrollView(
                child: NotLoggedInScreen(),
              ),
            ),
    );
  }
}

class SingleOrder extends StatelessWidget {
  const SingleOrder({
    Key? key,
    required this.index,
    required this.orderController,
  }) : super(key: key);

  final OrderController orderController;
  final index;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          title: Text(
            "#${orderController.custOrder[index].id}",
            style:
                TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          subtitle: Text(
            "Sunday, August 8",
            style: TextStyle(
                fontSize: 17,
                color: Colors.black,
                fontWeight: FontWeight.w300),
          ),
          trailing: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Rs. ",
                  style: TextStyle(
                      fontSize: 17,
                      color: Colors.black,
                      fontWeight: FontWeight.w400),
                ),
                TextSpan(
                  text: orderController.custOrder[index].items[0].payPrice
                      .toString()+"\n",
                  style: TextStyle(
                      fontSize: 18,
                      color: primaryColor,
                      fontWeight: FontWeight.bold),
                ),
                TextSpan(
                  text:
                      orderController.custOrder[index].orderStatus[0].type.toString(),
                  style: TextStyle(
                      fontSize: 17,
                      color: Colors.black,
                      fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
          shape: RoundedRectangleBorder(
            side: BorderSide(
                width: 1, color: Colors.purple.withOpacity(0.5)),
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
      ),
    );
  }
}

