import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/addAddress/addAddress.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

class SavedAddress extends StatefulWidget {
  const SavedAddress({Key? key}) : super(key: key);

  static String route = "savedAddress";

  @override
  _SavedAddressState createState() => _SavedAddressState();
}

class _SavedAddressState extends State<SavedAddress> {
  @override
  void initState() {
    final addressController =
        Provider.of<AddressController>(context, listen: false);
    addressController.fetchUserAddress();
  }

  @override
  Widget build(BuildContext context) {
    final addressController = Provider.of<AddressController>(context);
    var addresses = addressController.userAddress.address;
    return Scaffold(
      appBar: appBarBack(context, "Saved Address", () {
        Navigator.pop(context);
      }),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(8),
              child: Text(
                "Addresses",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: addresses.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                leading: Icon(Icons.home),
                title: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: addresses[index].name.toString() +
                            ", " +
                            addresses[index].address.toString() +
                            "\n",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.w500),
                      ),
                      TextSpan(
                          text: addresses[index].district.toString() +
                              ", " +
                              addresses[index].province.toString() +
                              ", " +
                              addresses[index].zipCode.toString() +
                              "\n",
                          style:
                              TextStyle(color: Colors.black87, fontSize: 15)),
                      TextSpan(
                          text: addresses[index].mobileNumber.toString() +
                              ", " +
                              addresses[index].alternatePhone.toString() +
                              "\n",
                          style: TextStyle(color: Colors.black87, fontSize: 15))
                    ],
                  ),
                ),
                trailing: PopupMenuButton(
                  icon: Icon(Icons.more_vert_rounded),
                  onSelected: (value) {
                    if (value == 2) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => addresses.length!=1 ? alertDialogGlobal(
                                  context,
                                  "Are you sure you want to delete this address?",
                                  () async {
                                await addressController
                                    .deleteAddress(addresses[index].id);
                                if (addressController.removeAdd ==
                                    "Address Deleted") {
                                  Navigator.pop(context);
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text("Failed to delete address")),
                                  );
                                }
                              },):AlertDialog(
                                content: Text("You can't delete last address"),
                              ));
                    } else {
                      // print("Pressed");
                    }
                  },
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      child: Text("Edit Address"),
                      value: 1,
                    ),
                    PopupMenuItem(
                      child: Text("Delete Address"),
                      value: 2,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomSheet: BottomAppBar(
        child: Container(
          width: double.infinity,
          height: 70,
          padding: EdgeInsets.all(16),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, AddAddress.route, arguments: "fromSavedAddress");
            },
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  child: Icon(
                    LineIcons.home,
                    color: Colors.white,
                  ),
                  color: Color(0xFF390c4d).withOpacity(0.8),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 73, vertical: 8),
                  child: Center(
                    child: Text(
                      "Add new Address",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                  color: primaryColor,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}




// onTap: () {
//                                  
//                                 },