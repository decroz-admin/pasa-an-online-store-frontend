import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/Widgets/product_block.dart';
import 'package:frontend_ui/views/MainComponents/drawer_global.dart';
import 'package:frontend_ui/views/MainComponents/reusableComponents.dart';
import 'package:frontend_ui/views/SingleProductScreen/single_product_screen.dart';
import 'package:provider/provider.dart';

class AllProducts extends StatefulWidget {
  const AllProducts({Key? key}) : super(key: key);
  static String route = "allproducts";

  @override
  _AllProductsState createState() => _AllProductsState();
}

class _AllProductsState extends State<AllProducts> {
  @override
  void initState() {
    final productsItem = Provider.of<ProductController>(context, listen: false);
    productsItem.products(context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final productsItem = Provider.of<ProductController>(context);

    // print(productsItem);
    return Scaffold(
      appBar: odrinaryAppbar("My Cart", Icons.search_outlined),
      drawer: AppDrawer(),
      body: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 190 / 250,
        children: productsItem.loading == true
            ? [
                Container(
                  child: Text("loading"),
                )
              ]
            : List.generate(
                productsItem.productsList.length,
                (index) => InkWell(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      SingleProductScreen.route,
                      arguments: productsItem.productsList[index].id,
                    );
                  },
                  child: ProductBlock(
                    id: productsItem.productsList[index].id,
                    name: productsItem.productsList[index].name,
                    description: productsItem.productsList[index].description,
                    price: productsItem.productsList[index].price,
                    quantity: productsItem.productsList[index].quantity,
                    productImages:
                        productsItem.productsList[index].productImages,
                  ),
                ),
              ),
      ),
    );
  }
}
