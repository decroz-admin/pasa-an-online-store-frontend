import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/LoginScreen/login_screen.dart';

class SignInBtn extends StatelessWidget {
  const SignInBtn({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      padding: EdgeInsets.symmetric(vertical: 25),
      width: double.infinity,
      child: TextButton(
        onPressed: () {
          Navigator.pushNamed(context, LoginScreen.route);
          // print("Successfully navigated to Signin Page");
        },
        style: TextButton.styleFrom(
          primary: Colors.black,
          backgroundColor: primaryColor,
          elevation: 5,
        ),
        child: Text(
          "Sign in",
          style: TextStyle(
              fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18),
        ),
      ),
    );
  }
}

class ImageArea extends StatelessWidget {
  const ImageArea({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Image.asset(
        "assets/a.png",
        scale: 1.75,
      ),
    );
  }
}
