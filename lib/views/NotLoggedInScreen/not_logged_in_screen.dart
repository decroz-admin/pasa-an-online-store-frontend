import 'package:flutter/material.dart';
import 'package:frontend_ui/views/NotLoggedInScreen/components/notLggedInContains.dart';

class NotLoggedInScreen extends StatelessWidget {
  const NotLoggedInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ImageArea(),
        Text(
          "Login to unlock all the features!",
          style: TextStyle(fontSize: 18),
        ),
        SignInBtn()
      ],
    );
  }
}
