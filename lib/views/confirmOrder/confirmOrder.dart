import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/MainComponents/bottombar.dart';
import 'package:line_icons/line_icons.dart';

class ConfirmedOrder extends StatelessWidget {
  const ConfirmedOrder({Key? key}) : super(key: key);

  static String route = "confirmOrder";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          Positioned(
            top: 20,
            right: 20,
            child: InkWell(
              onTap: (){
                Navigator.popAndPushNamed(context, BottomNavBar.route);
              },
              child: Text(
                "x",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
          ),
          Column(
            children: [
              Spacer(),
              Image.asset(
                "assets/hooray.png",
                scale: 7,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                " Your Order has been confirmed!",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Spacer(),
              Text(
                " Track Order: 12548",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    color: primaryColor),
              ),
              Spacer(),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Enjoy Pasa?\n",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    TextSpan(
                        text:
                            "Please share with us your experience by leving a review on play Store Pasa?",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                        )),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(16),
                child: GestureDetector(
                  onTap: () {
                    // Navigator.pushNamed(context, CheckoutPayment.route);
                  },
                  child: Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(8),
                        child: Icon(
                          LineIcons.star,
                          color: Colors.white,
                        ),
                        color: Color(0xFF390c4d).withOpacity(0.8),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 113, vertical: 8),
                        child: Center(
                          child: Text(
                            "Rate us",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        color: primaryColor,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    ));
  }
}
