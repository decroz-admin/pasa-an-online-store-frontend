import 'dart:convert';

import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/cart.dart';
import 'package:frontend_ui/models/category.dart';
import 'package:frontend_ui/models/orders.dart';
import 'package:frontend_ui/models/products.dart';
import 'package:frontend_ui/models/userAddresses.dart';
import 'package:http/http.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final storage = new FlutterSecureStorage();
String access_token = "";

accessToken() async {
  access_token = await storage.read(key: "token") as String;
  return access_token;
}

deleteToken() async {
  await accessToken();
  final response = await post(
    Uri.parse(url + "common/signout"),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 200) {
    await storage.deleteAll();
  } else {
    throw Exception('Failed to log out');
  }
}

Future<List<Products>> fetchPosts() async {
  final response = await get(Uri.parse(url + "common/getProducts"));
  if (response.statusCode == 200) {
    // print(response.body);
    List<Products> myProducts = productsFromJson(response.body);
    return myProducts;
  } else {
    throw Exception('Failed to load products data');
  }
}

Future<List<CategoryList>> fetchCategories() async {
  final response = await get(Uri.parse(url + "common/getCategories"));
  if (response.statusCode == 200) {
    Category category = categoryFromJson(response.body);
    //  print(category.categoryList);
    return category.categoryList;
  } else {
    throw Exception('Failed to products categories');
  }
}

Future<Cart> fetchCartItems() async {
  await accessToken();
  // print(token);
  final response = await get(
    Uri.parse(url + "user/getCartItems"),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  // print(response.body);
  if (response.statusCode == 201) {
    // List<Cart> carts = <Cart>[];
    // carts = await getCartListFromJson("");
    // print(response.body);
    // Map<String, dynamic>? body = jsonDecode(response.body);
    // print(body);
    Cart cart = cartFromJson(response.body);
    // print(cart);
    return cart;
  } else {
    throw Exception('Failed to load cart');
  }
}

// {"cartItems":{"60e27012c4cabf46600a8e01":{"_id":"60e27012c4cabf46600a8e01","name":"Ssamsung Galaxy S200000","price":800000,"qty":8},"60d1640f6dc09024046fb3ff":{"_id":"60d1640f6dc09024046fb3ff","name":"Ssamsung Galaxy A51","price":800000,"qty":16}}}
Future<String> userLogin(String email, String password) async {
  var response = await post(Uri.parse(url + "user/login"), body: {
    'email': email,
    'password': password,
  });
  if (response.statusCode == 200) {
    Map<String, dynamic> result = jsonDecode(response.body);
    String token = result['token'];

    await storage.write(key: "token", value: token);
    await storage.read(key: "token");
    return "login Success";
  } else {
    Map<String, dynamic> result = jsonDecode(response.body);
    String message = result['message'];
    return message;
    // throw Exception('Unable to login');
  }
}

Future<String> userSignup(String fname, String lname, String email,
    String password, String contact) async {
  var response = await post(Uri.parse(url + "user/signup"), body: {
    'firstName': fname,
    'lastName': lname,
    'email': email,
    'password': password,
    'contact_no': contact,
  });
  if (response.statusCode == 201) {
    return "SignUp Successful";
  } else {
    Map<String, dynamic> result = jsonDecode(response.body);
    String message = result['message'];
    return message;
    // throw Exception('Unable to login');
  }
}

//==================Remove CartItems==================================
Future<String> removeCartItems(String product) async {
  await accessToken();
  Map<String, dynamic> body = {
    "cartItems": [
      {"product": product}
    ]
  };
  String encodedData = json.encode(body);
  var response = await patch(
    Uri.parse(url + "user/removeCartItems"),
    body: encodedData,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 202) {
    return "Item Deleted";
  } else {
    Map<String, dynamic> result = jsonDecode(response.body);
    String message = result['message'];
    return message;
  }
}

//==================Update CartItems==================================
// Future<String> updateCartItems(String user, String product, String quantity) async {
// await accessToken();
//   var response = await patch(
//     Uri.parse(url + "/user/updateCartItems"),
//     body: {"user": userId, "product": productId},
// headers: {
//       'Content-Type': 'application/json',
//       'Accept': 'application/json',
//       'Authorization': 'Bearer $access_token',
//     },
//   );
//   if (response.statusCode == 200) {
//     return "Cart Items Successfullly Updated!";
//   } else {
//     Map<String, dynamic> result = jsonDecode(response.body);
//     String message = result['message'];
//     return message;
//   }
// }

//==================Add Items to Cart==================================
Future<String> addItemToCart(String productId, int quantity) async {
  await accessToken();
  Map<String, dynamic> body = {
    "cartItems": {"product": productId, "quantity": quantity}
  };

  String encodedData = json.encode(body);
  var response = await post(
    Uri.parse(url + "user/addToCart"),
    body: encodedData,
    headers: {
      'Content-Type': 'application/json',
      // 'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 201) {
    return "Items Successfully added to cart";
  } else {
    return "Failed to add item to cart";
  }
}

//==========================================fetch address========================================================
Future<UserAddress> fetchAddresses() async {
  await accessToken();
  final response = await get(
    Uri.parse(url + "common/getAddresses"),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 200) {
    UserAddress userAddress = userAddressFromJson(response.body);
    // print(userAddress);
    return userAddress;
  } else {
    throw Exception('Failed to load user address');
  }
}
//============================================================================================================================

//==========================================add address========================================================
Future<String> addAddresses(
  String name,
  String zipCode,
  String address,
  String province,
  String district,
  String mobileNumber,
  String alternatePhone,
  String addressType,
) async {
  await accessToken();

  Map<String, dynamic> body = {
    "payload": {
      "address": {
        "name": name,
        "zipCode": zipCode,
        "address": address,
        "province": province,
        "district": district,
        "mobileNumber": mobileNumber,
        "alternatePhone": alternatePhone,
        "addressType": addressType
      }
    }
  };
  final response = await post(
    Uri.parse(url + "common/addAddress"),
    body: json.encode(body),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 201) {
    return "Added Successful";
  } else {
    return "Failed to add";
  }
}

//============================================================================================================================

//==================Remove Address==================================
Future<String> removeAddress(String addressId) async {
  await accessToken();
  Map<String, dynamic> body = {
    "address": [
      {"_id": addressId}
    ]
  };

  String encodedData = json.encode(body);
  var response = await patch(
    Uri.parse(url + "common/deleteAddress"),
    body: encodedData,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 202) {
    return "Address Deleted";
  } else {
    Map<String, dynamic> result = jsonDecode(response.body);
    String message = result['message'];
    return message;
  }
}

//==========================================add address========================================================
Future<String> updateAddress(
  String _id,
  String name,
  String zipCode,
  String address,
  String province,
  String district,
  String mobileNumber,
  String alternatePhone,
  String addressType,
) async {
  await accessToken();

  Map<String, dynamic> body = {
    "user": "611e6b842f234543043df92a",
    "address": [
      {
        "_id": _id,
        "name": name,
        "zipCode": zipCode,
        "address": address,
        "province": province,
        "district": district,
        "mobileNumber": mobileNumber,
        "alternatePhone": alternatePhone,
        "addressType": addressType,
      }
    ]
  };
  final response = await patch(
    Uri.parse(url + "common/updateAddress"),
    body: json.encode(body),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 201) {
    return "Added Successful";
  } else {
    return "Failed to add";
  }
}

//============================================================================================================================

//==============================================add orders==================================================================================
Future<String> addOrders(
  String addressId,
  int totalAmount,
  String khaltiToken,
  String productId,
  int amount,
  int qty,
  String paymentType,
  String paymentStatus,
  String deliveryDate,
) async {
  await accessToken();

  Map<String, dynamic> body = {
    "addressId": addressId,
    "totalAmount": totalAmount,
    "token": khaltiToken,
    "items": [
      {"productId": productId, "payPrice": amount, "purchasedQty": qty}
    ],
    "paymentType": paymentType,
    "paymentStatus": paymentStatus,
    "deliveryDate": deliveryDate
  };
  final response = await post(
    Uri.parse(url + "user/addOrder"),
    body: json.encode(body),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 200) {
    return "Added Successful";
  } else {
    return "Failed to add";
  }
}

//====================================================fetch orders=======================================================
Future<List<Order>> getOrders() async {
  await accessToken();
  final response = await get(
    Uri.parse(url + "user/getOrders"),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 200) {
    List<Order> customersOrders = orderFromJson(response.body);
    return customersOrders;
  } else {
    throw Exception('Failed to load orders data');
  }
}
//======================================================================================================================================================================

Future<String> addReviews(
    double rating, String review, String productId) async {
  await accessToken();
  Map<String, dynamic> body = {
    "reviews": {"review": review, "rating": rating}
  };
  String encodedData = json.encode(body);
  var response = await patch(
    Uri.parse(url + "user/addReviews/${productId}"),
    body: encodedData,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $access_token',
    },
  );
  if (response.statusCode == 200) {
    return "Reviewed Successful";
  } else {
    return "Failed to add review";
  }
}
