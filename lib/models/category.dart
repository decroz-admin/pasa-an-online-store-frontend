// To parse this JSON data, do
//
//     final category = categoryFromJson(jsonString);

import 'dart:convert';

Category categoryFromJson(String str) => Category.fromJson(json.decode(str));

String categoryToJson(Category data) => json.encode(data.toJson());

class Category {
  Category({
    required this.categoryList,
  });

  List<CategoryList> categoryList;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        categoryList: List<CategoryList>.from(
            json["categoryList"].map((x) => CategoryList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "categoryList": List<dynamic>.from(categoryList.map((x) => x.toJson())),
      };
}

class CategoryList {
  CategoryList({
    required this.id,
    required this.name,
    required this.slug,
    required this.children,
    required this.categoryImage,
    required this.parentId,
  });

  String id;
  String name;
  String slug;
  List<CategoryList> children;
  String categoryImage;
  String parentId;

  factory CategoryList.fromJson(Map<String, dynamic> json) => CategoryList(
        id: json["_id"],
        name: json["name"],
        slug: json["slug"],
        children: List<CategoryList>.from(
            json["children"].map((x) => CategoryList.fromJson(x))),
        categoryImage:
            json["categoryImage"] == null ? null : json["categoryImage"],
        parentId: json["parentId"] == null ? null : json["parentId"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "slug": slug,
        "children": List<dynamic>.from(children.map((x) => x.toJson())),
        "categoryImage": categoryImage == null ? null : categoryImage,
        "parentId": parentId == null ? null : parentId,
      };
}
