// To parse this JSON data, do
//
//     final products = productsFromJson(jsonString);

import 'dart:convert';

List<Products> productsFromJson(String str) =>
    List<Products>.from(json.decode(str).map((x) => Products.fromJson(x)));

String productsToJson(List<Products> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Products {
  Products({
    required this.id,
    required this.name,
    required this.slug,
    required this.description,
    required this.quantity,
    required this.price,
    required this.category,
    required this.createdBy,
    required this.productImages,
    required this.reviews,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.idx,
  });

  String id;
  String name;
  String slug;
  String description;
  int quantity;
  int price;
  String category;
  String createdBy;
  List<ProductImage> productImages;
  List<Review> reviews;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  int idx;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
        id: json["_id"],
        name: json["name"],
        slug: json["slug"],
        description: json["description"],
        quantity: json["quantity"],
        price: json["price"],
        category: json["category"],
        createdBy: json["createdBy"],
        productImages: List<ProductImage>.from(
            json["productImages"].map((x) => ProductImage.fromJson(x))),
        reviews:
            List<Review>.from(json["reviews"].map((x) => Review.fromJson(x))),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        idx: json["idx"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "slug": slug,
        "description": description,
        "quantity": quantity,
        "price": price,
        "category": category,
        "createdBy": createdBy,
        "productImages":
            List<dynamic>.from(productImages.map((x) => x.toJson())),
        "reviews": List<dynamic>.from(reviews.map((x) => x.toJson())),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "idx": idx
      };
}

class ProductImage {
  ProductImage({
    required this.id,
    required this.img,
  });

  String id;
  String img;

  factory ProductImage.fromJson(Map<String, dynamic> json) => ProductImage(
        id: json["_id"],
        img: json["img"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "img": img,
      };
}

class Review {
  Review({
    required this.id,
    required this.userId,
    required this.review,
    required this.rating,
  });

  final String id;
  final String userId;
  final String review;
  final double rating;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
      id: json["_id"],
      userId: json["userId"],
      review: json["review"],
      rating: json["rating"] is int
          ? (json['rating'] as int).toDouble()
          : json['rating']);

  Map<String, dynamic> toJson() => {
        "_id": id,
        "userId": userId,
        "review": review,
        "rating": rating,
      };
}
