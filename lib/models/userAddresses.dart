// To parse this JSON data, do
//
//     final userAddress = userAddressFromJson(jsonString);

import 'dart:convert';

UserAddress userAddressFromJson(String str) =>
    UserAddress.fromJson(json.decode(str));

String userAddressToJson(UserAddress data) => json.encode(data.toJson());

class UserAddress {
  UserAddress({
    required this.id,
    required this.user,
    required this.v,
    required this.address,
    required this.createdAt,
    required this.updatedAt,
  });

  String id;
  String user;
  int v;
  List<Address> address;
  DateTime createdAt;
  DateTime updatedAt;

  factory UserAddress.fromJson(Map<String, dynamic> json) => UserAddress(
        id: json["_id"],
        user: json["user"],
        v: json["__v"],
        address:
            List<Address>.from(json["address"].map((x) => Address.fromJson(x))),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "user": user,
        "__v": v,
        "address": List<dynamic>.from(address.map((x) => x.toJson())),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
      };
}

class Address {
  Address({
    required this.id,
    required this.name,
    required this.zipCode,
    required this.address,
    required this.province,
    required this.district,
    required this.mobileNumber,
    required this.alternatePhone,
    required this.addressType,
  });

  String id;
  String name;
  String zipCode;
  String address;
  String province;
  String district;
  String mobileNumber;
  int alternatePhone;
  String addressType;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        id: json["_id"],
        name: json["name"],
        zipCode: json["zipCode"],
        address: json["address"],
        province: json["province"],
        district: json["district"],
        mobileNumber: json["mobileNumber"],
        alternatePhone: json["alternatePhone"],
        addressType: json["addressType"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "zipCode": zipCode,
        "address": address,
        "province": province,
        "district": district,
        "mobileNumber": mobileNumber,
        "alternatePhone": alternatePhone,
        "addressType": addressType,
      };
}
