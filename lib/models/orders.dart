// To parse this JSON data, do
//
//     final order = orderFromJson(jsonString);

import 'dart:convert';

List<Order> orderFromJson(String str) {
  var deoded = json.decode(str);
  // print(deoded);
  return List<Order>.from(deoded["orders"].map((x) => Order.fromJson(x)));
}

String orderToJson(List<Order> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Order {
  Order({
    required this.id,
    required this.items,
    required this.paymentType,
    required this.paymentStatus,
    required this.orderStatus,
  });

  String id;
  List<Item> items;
  PaymentType? paymentType;
  PaymentStatus? paymentStatus;
  List<OrderStatus> orderStatus;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
        id: json["_id"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        paymentType: paymentTypeValues.map[json["paymentType"]],
        paymentStatus: paymentStatusValues.map[json["paymentStatus"]],
        orderStatus: List<OrderStatus>.from(
            json["orderStatus"].map((x) => OrderStatus.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "paymentType": paymentTypeValues.reverse[paymentType],
        "paymentStatus": paymentStatusValues.reverse[paymentStatus],
        "orderStatus": List<dynamic>.from(orderStatus.map((x) => x.toJson())),
      };
}

class Item {
  Item({
    required this.id,
    required this.productId,
    required this.payPrice,
    required this.purchasedQty,
  });

  String id;
  ProductId productId;
  int payPrice;
  int purchasedQty;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["_id"],
        productId: ProductId.fromJson(json["productId"]),
        payPrice: json["payPrice"],
        purchasedQty: json["purchasedQty"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "productId": productId.toJson(),
        "payPrice": payPrice,
        "purchasedQty": purchasedQty,
      };
}

class ProductId {
  ProductId({
    required this.id,
    required this.name,
  });

  Id? id;
  Name? name;

  factory ProductId.fromJson(Map<String, dynamic> json) => ProductId(
        id: idValues.map[json["_id"]],
        name: nameValues.map[json["name"]],
      );

  Map<String, dynamic> toJson() => {
        "_id": idValues.reverse[id],
        "name": nameValues.reverse[name],
      };
}

enum Id { THE_60_E27012_C4_CABF46600_A8_E01, THE_60_E19_C0_EEEEC5_D4730866713 }

final idValues = EnumValues({
  "60e19c0eeeec5d4730866713": Id.THE_60_E19_C0_EEEEC5_D4730866713,
  "60e27012c4cabf46600a8e01": Id.THE_60_E27012_C4_CABF46600_A8_E01
});

enum Name { SSAMSUNG_GALAXY_S200000, SSAMSUNG_GALAXY_S200 }

final nameValues = EnumValues({
  "Ssamsung Galaxy S200": Name.SSAMSUNG_GALAXY_S200,
  "Ssamsung Galaxy S200000": Name.SSAMSUNG_GALAXY_S200000
});

class OrderStatus {
  OrderStatus({
    required this.type,
    required this.isCompleted,
    required this.id,
    required this.date,
  });

  Type? type;
  bool isCompleted;
  String id;
  DateTime? date;

  factory OrderStatus.fromJson(Map<String, dynamic> json) => OrderStatus(
        type: typeValues.map[json["type"]],
        isCompleted: json["isCompleted"],
        id: json["_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
      );

  Map<String, dynamic> toJson() => {
        "type": typeValues.reverse[type],
        "isCompleted": isCompleted,
        "_id": id,
        "date": date == null ? null : date!.toIso8601String(),
      };
}

enum Type { ORDERED, PACKED, SHIPPED, DELIVERED }

final typeValues = EnumValues({
  "delivered": Type.DELIVERED,
  "ordered": Type.ORDERED,
  "packed": Type.PACKED,
  "shipped": Type.SHIPPED
});

enum PaymentStatus { PENDING, COMPLETED }

final paymentStatusValues = EnumValues(
    {"completed": PaymentStatus.COMPLETED, "pending": PaymentStatus.PENDING});

enum PaymentType { COD, KHALTI }

final paymentTypeValues =
    EnumValues({"cod": PaymentType.COD, "khalti": PaymentType.KHALTI});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
