// // To parse this JSON data, do
// //
// //     final cart = cartFromJson(jsonString);

import 'dart:convert';

Cart cartFromJson(String str) => Cart.fromJson(json.decode(str));

String cartToJson(Cart data) => json.encode(data.toJson());

class Cart {
    Cart({
       required this.cartItems,
    });

    Map<String, CartItem> cartItems;

    factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        cartItems: Map.from(json["cartItems"]).map((k, v) => MapEntry<String, CartItem>(k, CartItem.fromJson(v))),
    );

    Map<String, dynamic> toJson() => {
        "cartItems": Map.from(cartItems).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
    };
}

class CartItem {
    CartItem({
      required  this.id,
      required  this.name,
      required  this.price,
      required  this.qty,
      
    });

    String id;
    String name;
    int price;
    int qty;
    String imag = "default.png";

    factory CartItem.fromJson(Map<String, dynamic> json) => CartItem(
        id: json["_id"],
        name: json["name"],
        price: json["price"],
        qty: json["qty"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "price": price,
        "qty": qty,
    };
}

// To parse this JSON data, do

//     final cart = cartFromJson(jsonString);

// import 'dart:convert';

// Cart cartFromJson(String str) => Cart.fromJson(json.decode(str));

// String cartToJson(Cart data) => json.encode(data.toJson());

// class Cart {
//     Cart({
//       required  this.id,
//       required  this.user,
//       required  this.cartItems,
//       required  this.createdAt,
//       required  this.updatedAt,
//       required  this.v,
//     });

//     String id;
//     String user;
//     List<CartItem> cartItems;
//     DateTime createdAt;
//     DateTime updatedAt;
//     int v;

//     factory Cart.fromJson(Map<String, dynamic> json) => Cart(
//         id: json["_id"],
//         user: json["user"],
//         cartItems: List<CartItem>.from(json["cartItems"].map((x) => CartItem.fromJson(x))),
//         createdAt: DateTime.parse(json["createdAt"]),
//         updatedAt: DateTime.parse(json["updatedAt"]),
//         v: json["__v"],
//     );

//     Map<String, dynamic> toJson() => {
//         "_id": id,
//         "user": user,
//         "cartItems": List<dynamic>.from(cartItems.map((x) => x.toJson())),
//         "createdAt": createdAt.toIso8601String(),
//         "updatedAt": updatedAt.toIso8601String(),
//         "__v": v,
//     };
// }

// class CartItem {
//     CartItem({
//       required  this.quantity,
//       required  this.id,
//       required  this.product,
//     });

//     int quantity;
//     String id;
//     String product;

//     factory CartItem.fromJson(Map<String, dynamic> json) => CartItem(
//         quantity: json["quantity"],
//         id: json["_id"],
//         product: json["product"],
//     );

//     Map<String, dynamic> toJson() => {
//         "quantity": quantity,
//         "_id": id,
//         "product": product,
//     };
// }

// // import 'dart:convert';

// // List<Cart> getCartListFromJson(String str) => List<Cart>.from(jsonDecode(str));
// // class Cart{
// //   String id;
// //   String name;
// //   int price;
// //   String qty;
// //   Cart({
// //     required this.name,
// //     required this.id,
// //     required this.price,
// //     required this.qty,
// //   });
// //   Cart.fromJson(Map<String, dynamic> json): this.name = json['name'], this.id =json['_id'], this.price = json['price'], this.qty = json['qty'];
// // }

// To parse this JSON data, do
//
//     final cart = cartFromJson(jsonString);

// import 'dart:convert';

// Cart cartFromJson(String str) => Cart.fromJson(json.decode(str));

// String cartToJson(Cart data) => json.encode(data.toJson());

// class Cart {
//   Cart({
//     required this.id,
//     required this.user,
//     required this.cartItems,
//     required this.createdAt,
//     required this.updatedAt,
//     required this.v,
//   });

//   String id;
//   String user;
//   List<CartItem> cartItems;
//   DateTime createdAt;
//   DateTime updatedAt;
//   int v;

//   factory Cart.fromJson(Map<String, dynamic> json) => Cart(
//         id: json["_id"],
//         user: json["user"],
//         cartItems: List<CartItem>.from(
//             json["cartItems"].map((x) => CartItem.fromJson(x))).toList(),
//         createdAt: DateTime.parse(json["createdAt"]),
//         updatedAt: DateTime.parse(json["updatedAt"]),
//         v: json["__v"],
//       );

//   Map<String, dynamic> toJson() => {
//         "_id": id,
//         "user": user,
//         "cartItems": List<dynamic>.from(cartItems.map((x) => x.toJson())),
//         "createdAt": createdAt.toIso8601String(),
//         "updatedAt": updatedAt.toIso8601String(),
//         "__v": v,
//       };
// }

// class CartItem {
//   CartItem({
//     required this.quantity,
//     required this.id,
//     required this.product,
//   });

//   int quantity;
//   String id;
//   String product;

//   factory CartItem.fromJson(Map<String, dynamic> json) => CartItem(
//         quantity: json["quantity"],
//         id: json["_id"],
//         product: json["product"],
//       );

//   Map<String, dynamic> toJson() => {
//         "quantity": quantity,
//         "_id": id,
//         "product": product,
//       };
// }
