import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontend_ui/services/apiServices.dart';

class VerificationChecker extends ChangeNotifier {
  bool loading = false;
  bool isloggedIn = false;

  final storage = new FlutterSecureStorage();

  //logged in Check
  isLoggedIn() async {
    loading = true;
    bool is_LoggedIn = await storage.containsKey(key: 'token');
    // print(await storage.containsKey(key: 'token'));
    isloggedIn = is_LoggedIn;
    loading = false;
    notifyListeners();
  }

  isloggetOut() async {
    loading = true;
    loading = false;
    await deleteToken();
    isloggedIn = false;
    notifyListeners();
  }
}
