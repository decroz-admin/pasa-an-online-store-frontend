import 'package:flutter/cupertino.dart';
import 'package:frontend_ui/models/orders.dart';
import 'package:frontend_ui/services/apiServices.dart';

class OrderController extends ChangeNotifier {
  bool loading = false;

  List<Order> custOrder = [];

  fetchOrder() async {
    loading = true;
    custOrder = await getOrders();
    loading = false;
    notifyListeners();
  }
}
