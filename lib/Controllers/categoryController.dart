import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:frontend_ui/models/category.dart';
import 'package:frontend_ui/services/apiServices.dart';

class CategoryController extends ChangeNotifier {
  List<CategoryList> categorylist = [];
  List<CategoryList> categoryByParentID = [];

  bool loading = false;

  category(context) async {
    loading = true;
    categorylist = await fetchCategories();
    loading = false;
    notifyListeners();
  }

  fetchCateChildren(String parentID) {
    loading = true;
    CategoryList myList= categorylist.firstWhere((element) => element.id == parentID);
    categoryByParentID = myList.children;
    loading = true;
    notifyListeners();
  }
}
