import 'package:flutter/material.dart';
import 'package:frontend_ui/models/category.dart';
import 'package:frontend_ui/models/products.dart';
import 'package:frontend_ui/services/apiServices.dart';

class ProductController extends ChangeNotifier {
  List<Products> productsList = [];
  List<Products> categorylistbyId = [];
  List<Products> latestAdded = [];
  List<Products> dateFilteredProduct = [];

  double rate = 2.5;
  String review = '';
  rating(double value) {
    rate = value;
    notifyListeners();
  }

  reviews(String value) {
    review = value;
    notifyListeners();
  }

  bool loading = false;

  products(context) async {
    loading = true;
    productsList = await fetchPosts();
    loading = false;
    notifyListeners();
  }

  recentlyAdded() async {
    loading = true;
    productsList = await fetchPosts();
    productsList.sort((a, b) => b.createdAt.compareTo(a.createdAt));
    loading = false;
    notifyListeners();
  }

//    findProductsByCategory(String categoryId) async{
//     await products(null);
//     List<Products> productsByCategory = productsList.where((element) {
//       // print(  element.category.toLowerCase().contains(
//       //       categoryId.toLowerCase(),)
//       //     );
//       return  element.category.toLowerCase().contains(
//             categoryId.toLowerCase(),
//           );
//     }).toList();
//     // print("bbb");
//  categorylistbyId = productsByCategory;
//     // print(productsByCategory);
//     // print("aaa");
//   }
}

class ProductByCategoriesController extends ChangeNotifier {
  List<Products> proByCate = [];
  List<Products> proById = [];
  List<Products> proByIds = [];

  List<Products> allProducts = [];
  List<Products> recommendedProducts = [];
  bool loading = false;

  fetchRecommended(context, int idx) {
    loading = true;
    recommendedProducts = [];
    // print("This is an idx");
    // print(idx);

    recommendedProducts = allProducts.where((element) {
      // print("Here should be multiple idx");
      // print(element.idx);
      return element.idx == idx;
    }).toList();
    print("from controller");
    print(recommendedProducts);
    loading = false;
  }

  findAllProductOfId(List<CategoryList> allcategory) async {
    loading = true;
    allProducts = await fetchPosts();
    List<String> allCategoryIds = [];
    allCategoryIds = allcategory.map((e) => e.id).toList();
    proByIds = allProducts
        .where((element) => allCategoryIds.contains(element.category))
        .toList();
    loading = false;
    notifyListeners();
  }

  findProductsByCate(context, String cateId) async {
    loading = true;
    proByCate = await fetchPosts();

    List<Products> productsCate = proByCate;

    proByCate = productsCate.where((element) {
      return element.category.toLowerCase().contains(cateId.toLowerCase());
    }).toList();
    loading = false;
    notifyListeners();
  }

  findProductsById(context, String productId) async {
    loading = true;
    proById = await fetchPosts();

    List<Products> productsId = proById;

    proById = productsId.where((element) {
      return element.id == productId;
    }).toList();
    loading = false;
    // print(proById);
    notifyListeners();
  }
}
