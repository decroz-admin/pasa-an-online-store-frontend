import 'package:flutter/material.dart';
import 'package:frontend_ui/models/cart.dart';
import 'package:frontend_ui/models/products.dart';
import 'package:frontend_ui/services/apiServices.dart';

class CartController extends ChangeNotifier {
  // Cart cart = new Cart(cartItems: {});
  List<CartItem> cartItems = [];
  bool loading = false;
  List<Products> productlist = [];
  String removeItem = '';
  int totalPrice = 0;
  int _counterValue = 1;
  Map<String, int> currentValues = {"some": 1};
  String someID = "";
  // int get totalPrice => _totalPrice;

  // set totalPrice(int total) {
  //   _totalPrice = total;
  //   notifyListeners();
  // }

  totalPriceProdut(int total) {
    loading = true;
    totalPrice = total;
    loading = false;
    // notifyListeners();
  }

  setSomeId(String id) {
    loading = true;
    someID = id;
    loading = false;
    notifyListeners();
  }

  int get counterValue => _counterValue;

  set qtyCounterValue(int qty) {
    _counterValue = qty;
    notifyListeners();
  }

  increaseQty() {
    loading = true;
    _counterValue++;
    loading = false;
    notifyListeners();
  }

  decreaseQty() {
    loading = true;
    _counterValue--;
    loading = false;
    notifyListeners();
  }

  increaseQtybyId(String id) {
    loading = true;
    int value = currentValues[id] as int;
    currentValues[id] = value + 1;
    loading = false;
    notifyListeners();
  }

  decreaseQtybyId(String id) {
    loading = true;
    int value = currentValues[id] as int;
    if (value > 1) {
      currentValues[id] = value - 1;
    }
    loading = false;
    notifyListeners();
  }

  setQtyById(String id) {
    currentValues[id] = 0;
  }

  fetchCart(context) async {
    loading = true;
    cartItems = [];

    Cart cart = await fetchCartItems();
    cart.cartItems.forEach((key, value) => cartItems.add(value));
    productlist = await fetchPosts();
    cart.cartItems.forEach((key, value) {
      currentValues[value.id] = value.qty;
    });
    productlist.forEach((element) {
      for (int i = 0; i < cartItems.length; i++) {
        if (element.id == cartItems[i].id) {
          //  cartItems[i].imag = (element.productImages[0] == null ? "default.png": element.productImages[0].img);
          if (element.productImages.length == 0) {
            cartItems[i].imag = "default.png";
          } else {
            cartItems[i].imag = element.productImages[0].img;
          }
        }
      }
    });
// print(cartItems[0].imag);
    loading = false;
    notifyListeners();
  }
//================================================================================================================================

// ==================================================== fetch product Image =========================================
  fetchPoductImage(context) async {
    loading = true;
    productlist = await fetchPosts();
    productlist.forEach((element) {
      for (int i = 0; i < cartItems.length; i++) {
        if (element.id == cartItems[i].id) {
          cartItems[i].imag = element.productImages[0].img;
        }
      }
    });
  }
//==================================================================================================================================================================

//==============================================add items to cart========================================================================
  addItemsToCart(String productId, int qty) async {
    loading = true;
    final addedItems = await addItemToCart(productId, qty);
    loading = false;
    notifyListeners();
  }
//==================================================================================================================================================================

//==========================================================delete items of cart ========================================================================
  deleteItemsCart(String productId) async {
    loading = true;
    removeItem = await removeCartItems(productId);
    if (this.removeItem == "Item Deleted") {
      this.cartItems.removeWhere((element) => element.id == productId);
    }
    loading = false;
    notifyListeners();
  }

//==================================================================================================================================================================

}
