import 'package:flutter/cupertino.dart';
import 'package:frontend_ui/services/apiServices.dart';

enum Payment { COD, Khalti }

class CheckoutController extends ChangeNotifier {
  bool showCartItems = true;
  Payment payment = Payment.COD;
  bool loading = false;
  String addedOrders = "";
  String dateDelivery ="";

   delivery (value){
    dateDelivery = value;
    // notifyListeners();
  }


   paymentType(value) {
    payment = value;
    notifyListeners();
  }

   showList() {
    showCartItems = !showCartItems;
    notifyListeners();
  }

  addOrderItems(
    String addressId,
    int totalAmount,
    String khaltiToken,
    String productId,
    int amount,
    int qty,
    String paymentType,
    String paymentStatus,
    String deliveryDate,
  ) async {
    loading = true;
    addedOrders = await addOrders(addressId, totalAmount, khaltiToken,
        productId, amount, qty, paymentType, paymentStatus, deliveryDate);
    loading = false;
    notifyListeners();
  }
}
