import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeController extends ChangeNotifier {
  ThemeData? themeData;

  setTheme(ThemeData theme) {
    themeData = theme;
    notifyListeners();
  }
}
