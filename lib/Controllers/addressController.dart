import 'package:flutter/material.dart';
import 'package:frontend_ui/models/userAddresses.dart';
import 'package:frontend_ui/services/apiServices.dart';

class AddressController extends ChangeNotifier {
  bool loading = false;
  UserAddress userAddress = new UserAddress(
      id: "",
      user: "",
      v: 1,
      address: [],
      createdAt: DateTime.now(),
      updatedAt: DateTime.now());
  String currentId = '';
  String removeAdd = "";
  List<Address> singleuserAddress =[];
  // Address singleuserAddress = new Address(
  //     id: "id",
  //     name: "name",
  //     zipCode: "zipCode",
  //     address: "address",
  //     province: "province",
  //     district: "district",
  //     mobileNumber: "mobileNumber",
  //     alternatePhone: 123456789,
  //     addressType: "addressType");
  fetchUserAddress() async {
    loading = true;
    userAddress = await fetchAddresses();
    currentId = userAddress.address[0].id;
    loading = false;
    notifyListeners();
  }

  fetchAddress(String addressId) async {
    loading = true;
    singleuserAddress =
        userAddress.address.where((element) => element.id == addressId).toList();
    // print("ashish");
    // print(singleuserAddress);
    loading = false;
    // notifyListeners();
  }

  changeAddress(addressId) {
    loading = true;
    currentId = addressId;
    loading = false;
    notifyListeners();
  }

  deleteAddress(String addressId) async {
    loading = true;

    removeAdd = await removeAddress(addressId);
    if (this.removeAdd == "Address Deleted") {
      this
          .userAddress
          .address
          .removeWhere((element) => element.id == addressId);
      // print(userAddress.address[0].id);
    }
    loading = false;
    notifyListeners();
  }
}
