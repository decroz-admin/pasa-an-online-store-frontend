// @dart=2.9
import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/addressController.dart';
import 'package:frontend_ui/Controllers/cartController.dart';
import 'package:frontend_ui/Controllers/categoryController.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:frontend_ui/Controllers/navController.dart';
import 'package:frontend_ui/Controllers/orderController.dart';
import 'package:frontend_ui/Controllers/productController.dart';
import 'package:frontend_ui/Controllers/themeController.dart';
import 'package:frontend_ui/Controllers/verificatinChecker.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/routes.dart';
import 'package:frontend_ui/views/MainComponents/bottombar.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => ThemeController()),
    ChangeNotifierProvider(create: (_) => ProductController()),
    ChangeNotifierProvider(create: (_) => CategoryController()),
    ChangeNotifierProvider(create: (_) => ProductByCategoriesController()),
    ChangeNotifierProvider(create: (_) => CartController()),
    ChangeNotifierProvider(create: (_) => VerificationChecker()),
    ChangeNotifierProvider(create: (_) => CheckoutController()),
    ChangeNotifierProvider(create: (_) => AddressController()),
    ChangeNotifierProvider(create: (_) => OrderController()),
    ChangeNotifierProvider(create: (_) => NavController())
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final navController = Provider.of<NavController>(context);
    final themeController = Provider.of<ThemeController>(context);

    return MaterialApp(
      title: 'Flutter Demo',
      home: BottomNavBar(),
      routes: getRoutes(),
    );
  }
}
