import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/models/products.dart';

class ProductBlock extends StatelessWidget {
  final String description;
  final List<ProductImage> productImages;
  final String name;
  final int quantity;
  final int price;
  final String id;

  ProductBlock(
      {Key? key,
      required this.id,
      required this.name,
      required this.description,
      required this.price,
      required this.quantity,
      required this.productImages})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print(productImages);
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: InkWell(
        // onTap: () => Navigator.pushNamed(context, ProductDetails.routeName,
        //     arguments: productsAttributes.id),
        child: Container(
          width: 190,
          height: 250,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: Theme.of(context).backgroundColor),
          child: Column(
            children: [
              Column(
                children: [
                  Stack(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(2),
                        child: Container(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height * 0.26,
                          child: productImages.length == 0
                              ? Image.network(url + "image/default.png",                                  fit: BoxFit.fill)
                              : Image.network(
                                  (url + "image/" +productImages[0].img).toString(),
                                  fit: BoxFit.fill,
                                ),
                        ),
                      ),
                      // Positioned(
                      //   right: 10,
                      //   top: 8,
                      //   child: Icon(
                      //     Icons.star,
                      //     color: Colors.grey.shade800,
                      //   ),
                      // ),
                      // Positioned(
                      //   right: 10,
                      //   top: 8,
                      //   child: Icon(
                      //     Icons.star_outlined,
                      //     color: Colors.white,
                      //   ),
                      // ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: 8, top: 8),
                margin: EdgeInsets.only(left: 5, bottom: 2, right: 3),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "\Rs. ${price.toString()}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: primaryColor,
                              fontSize: 18),
                        ),
                        // Spacer(flex: 1,),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {},
                            borderRadius: BorderRadius.circular(30.0),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
                              child: Icon(
                                Icons.production_quantity_limits,
                                size: 25,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
