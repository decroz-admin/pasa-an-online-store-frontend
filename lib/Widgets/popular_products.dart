import 'package:flutter/material.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:frontend_ui/views/SingleProductScreen/single_product_screen.dart';

class PopularProducts extends StatefulWidget {
  const PopularProducts({Key? key, this.productAttrib}) : super(key: key);
  final productAttrib;

  @override
  _PopularProductsState createState() => _PopularProductsState();
}

class _PopularProductsState extends State<PopularProducts> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
      child: Container(
        width: 200,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(
              10.0,
            ),
            bottomRight: Radius.circular(10.0),
          ),
        ),
        child: InkWell(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(
              10.0,
            ),
            bottomRight: Radius.circular(10.0),
          ),
          onTap: () {
            // print("this is the length of recommended product");
            // print(widget.productAttrib.productImages[0].img);
            // print(" id of above idx");
            Navigator.popAndPushNamed(context, SingleProductScreen.route,
                arguments: widget.productAttrib.id);
            // DefaultTabController.of(context)!.animateTo(1);
            // print(widget.productAttrib.recommendedProducts[index].id);
          },
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: 170,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image:
                            // widget.productAttrib.productImages.length == 0?
                            NetworkImage(url +
                                "image/" +
                                widget.productAttrib.productImages[0].img),
                      ),
                      //         : NetworkImage(url + "image/default.png"),
                      // fit: BoxFit.fill)
                    ),
                  ),
                  Positioned(
                    right: 10,
                    top: 8,
                    child: Icon(
                      Icons.star,
                      color: Colors.grey.shade800,
                    ),
                  ),
                  Positioned(
                    right: 10,
                    top: 8,
                    child: Icon(
                      Icons.star_outlined,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.productAttrib.name,
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.productAttrib.price.toString(),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: primaryColor,
                              fontSize: 18),
                        ),
                        // Spacer(flex: 1,),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {},
                            borderRadius: BorderRadius.circular(30.0),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
                              child: Icon(
                                Icons.production_quantity_limits,
                                size: 25,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
