import 'package:flutter/material.dart';
import 'package:frontend_ui/Controllers/checkoutController.dart';
import 'package:frontend_ui/constraints.dart';
import 'package:provider/provider.dart';

class DeliveryDate extends StatefulWidget {
  DeliveryDate({Key? key}) : super(key: key);

  @override
  _DeliveryDateState createState() => _DeliveryDateState();
}

class _DeliveryDateState extends State<DeliveryDate> {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 30)));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    final checkoutController = Provider.of<CheckoutController>(context);
    checkoutController.delivery("${selectedDate.toLocal()}".split(' ')[0]);
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: 3.0,
        ),
      ),
      child: TextButton(
        onPressed: () => _selectDate(context),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              'Choose Delivery Date',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 18),
            ),
            SizedBox(
              width: 40,
            ),
            Text(
              "${selectedDate.toLocal()}".split(' ')[0],
              style: TextStyle(
                color: primaryColor,
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
